var type="";

function insertIngredient() {
   var per_pizza = "N";
   var per_panino = "N";
   var name = $("#nome").val();
   var euro = $("#euroPrice").val();
   if(document.getElementById("per_pizza").checked){
     per_pizza = "Y";
   }
   if(document.getElementById("per_panino").checked){
     per_panino = "Y";
   }
   if((name != "" && type != "" && euro != "") && (type != "ingrediente" ||(type == "ingrediente" && (per_pizza != "N" || per_panino != "N")))){
     var request = new XMLHttpRequest();
     request.onreadystatechange = function() {
       if ( request.readyState == 4 && request.status == 200 ){
         if(request.responseText != "Inserito nuovo ingrediente!") {
           createWrongModal("Nome già usato");
         } else {
           $(".modal-title").html(request.responseText);
           $("#modcontent").html("Inserimento " + name + " riuscito");
           $("#wrong > button").html("Aggiungi altro");
           $("#success").collapse("show");
           $("#wrong").collapse("show");
           reset();
         }
       }
     };
     request.open("GET","../php/dbInsertion/insertIngredient.php?ingrName="+name+"&type="+type+"&per_pizza="+per_pizza+"&per_panino="+per_panino+"&euro="+euro, true );
     request.send();
  } else {
    var str = checkForm(name, euro, per_pizza, per_panino);
    createWrongModal(str);
  }
}

function cambiaPagina() {
  window.location.href = "adminEntryPage.php";
}

function createWrongModal(str) {
  $(".modal-title").html(str);
  $("#modcontent").html("Inserimento non riuscito");
  $("#wrong > button").html("Torna al form");
  $("#success").collapse("hide");
  $("#wrong").collapse("show");
}

function changeType(ingType){
  type = ingType;
  if(type == "ingrediente"){
    $("#divScelta").show();
  } else {
    $("#per_pizza").prop("checked", false);
    $("#per_panino").prop("checked", false);
    $("#divScelta").hide();
  }
}

function checkForm(name, euro, per_pizza, per_panino) {
  var str = "";
  if(name == ""){
    str = "ATTENZIONE! Campo NOME vuoto!";
    if(type == "" || euro == "") {
      str = "ATTENZIONE! Riempire tutti i campi";
    }
  }
  if(type == ""){
    str = "ATTENZIONE! Scegliere un TIPO";
    if(name == "" || euro == "") {
      str = "ATTENZIONE! Riempire tutti i campi";
    }
  }
  if(euro == ""){
    str = "ATTENZIONE! Campo PREZZO vuoto o non è un numero";
    if(type == "" || name == "") {
      str = "ATTENZIONE! Riempire tutti i campi";
    }
  }
  if(type == "ingrediente" && (per_pizza == "N" && per_panino == "N")) {
    str = "ATTENZIONE! Scegliere se l'ingrediente deve essere usato per pizza, per panino o per entrambi";
  }
  return str;
}

function reset(){
  $("#nome").val("");
  $("#euroPrice").val("");
  $("#per_pizza").prop("checked", false);
  $("#per_panino").prop("checked", false);
  $("#divScelta").hide();
  $("#base_pizza").prop('checked', false);
  $("#base_panino").prop("checked", false);
  $("#ingr").prop("checked", false);
  $("#pasta").prop("checked", false);
  $("#sugo").prop("checked", false);
  $("#farina").prop("checked", false);
  $("#salsa").prop("checked", false);
}

$(document).ready(function() {
  $("#add").click(function(){
    insertIngredient();
  });
});
