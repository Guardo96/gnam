function rimuovi(id, index, tipo, table) {
  var url = "../php/removeQuery.php?id=" + id + "&tipo=" + tipo + "&table=" + table;
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    var idDiv = "";
    var container = "#divlist";
    var newText = "Nessun menù";
    if(table == "news") {
      container = "#divnews";
      newText = "Nessuna news";
    }
     if ( xhr.readyState == 4 && xhr.status == 200 ){
       if(table != "menu" && table != "news") {
         $("div").remove("#per" + index);
         $("div").remove("#daModificare" + index);
         if($("#coll" + tipo).html() == "") {
           $("#coll" + tipo).html(scegliStringa(tipo));
         }
       } else {
         $("div").remove(".per" + index);
         $("div").remove("#coll" + index);
         if($(container + " > div").html() == undefined) {
           $(container).html(newText);
         }
       }
     }
  };
  xhr.open("GET",url, true );
  xhr.send();
}

function scegliStringa(tipo) {
  var parola = "";
  elem = tipo;
  switch (tipo) {
    case "stuzzichino":
      parola = "Nessuno ";
      break;
    case "pizza":
    case "bibita":
    case "salsa":
      parola = "Nessuna ";
      break;
    case "base_pizza":
      parola = "Nessuna ";
      elem = "base per pizza";
      break;
    case "pasta":
    case "farina":
    case "pane":
      parola = "Nessun ";
      elem = "tipo di " + tipo;
      break;
    default:
      parola = "Nessun ";
      elem = tipo;
  }
  return parola + elem;
}

function modifica(id) {
  $("#daModificare" + id).collapse("show");
}
/*
function modificaSconto(id, index) {
  var val = $("#checksconto" + index).prop("checked");
  alert(val);
  var url = "../php/updateQuery.php?id=" + id + "&table=piatto&val=" + val + "&elem=sconto";
}
*/
function update(id, index, table) {
  var mod = "Prezzo";
  var newText = '<span>prezzo:</span> ';
  if(table == "elemento") {
    newText = '<span>sovrapprezzo:</span> ';
  }
  if(table == "news") {
    mod = "Testo";
    newText = '<span>testo:</span> ';
  }
  var val = $("#modifica" + mod + index).val();
  var url = "../php/updateQuery.php?id=" + id + "&table=" + table + "&val=" + val;
  var valsconto;
  if(table != "news") {
    if($("#checksconto" + index).prop("checked")) {
      valsconto = "Y";
    } else {
      valsconto ="N";
    }
    url = "../php/updateQuery.php?id=" + id + "&table=" + table + "&val=" + val + "&sconto=" + valsconto;
  }
  var xhr = new XMLHttpRequest();
  if(val != "") {
    xhr.onreadystatechange = function() {
       if ( xhr.readyState == 4 && xhr.status == 200 ){
         if(table != "news") {
           val = new Number(val);
           $("#" + index + " > #prezzo" + index).html(newText + val.toFixed(2) + '€<br/>');
         } else {
           $("#" + index + " > #testo" + index).html(newText + val + '<br/>');
         }
         if(table != "elemento" && table != "news") {
           checkSconto(valsconto == "Y", index);
         }
         $("#daModificare" + index).collapse("hide");
       }
    };
    xhr.open("GET",url, true );
    xhr.send();
  } else if(table != "elemento" && table != "news"){
    url = "../php/updateQuery.php?id=" + id + "&table=" + table + "&sconto=" + valsconto;
    alert(url);
    xhr.onreadystatechange = function() {
       if ( xhr.readyState == 4 && xhr.status == 200 ){
         checkSconto(valsconto == "Y", index);
         $("#daModificare" + index).collapse("hide");
       }
    };
    xhr.open("GET",url, true );
    xhr.send();
  }
}

function checkSconto(val, index) {
  var newText = '<span>sconto:</span> ';
  if(val) {
    $("#" + index + ">#sconto" + index).html(newText + "Y<br/>");
  } else {
    $("#" + index + ">#sconto" + index).html(newText + "N<br/>");
  }
}
