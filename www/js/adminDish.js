var formaggio = '<div class="custom-control custom-checkbox">' +
                '<input type="checkbox" id="formaggio" name="add" class="custom-control-input"/>' +
                '<label class="custom-control-label" for="formaggio">Aggiungi formaggio</label></div>';

var type="";

//Usata nel radio button per la scelta del tipo di piatto da creare
function selectDish(piatto, file) {
  modifyText($(piatto).val());
  type = piatto;
  sessionStorage.setItem("tipoPiatto", piatto);
  if ($(piatto).val() == "primo") {
    $("#divSecondo").collapse("hide");
    $("#divPrimo").collapse("show");
    $(".modificaPrezzo").collapse("show");
    $("#infoPrice").collapse("hide");
    ingredientRequest($(piatto).val(), "#base1", file);
    ingredientRequest($(piatto).val(), "#base2", file);
  } else {
    $("#divPrimo").collapse("hide");
    $("#divSecondo").collapse("hide");
    if ($(piatto).val() == "secondo") {
      $("#divPrimo").collapse("hide");
      $("#divSecondo").collapse("show");
      $("#infoPrice").collapse("hide");
      $(".modificaPrezzo").collapse("hide");
    } else {
      $("#divPrimo").collapse("hide");
      $("#divSecondo").collapse("hide");
      $("#infoPrice").collapse("show");
      $(".modificaPrezzo").collapse("hide");
    }
  }
  $("#infoName").collapse("show");
  $("#pizza").prop('checked', false);
  $("#panino").prop('checked', false);
  $("#divBSecondo").collapse("hide");
  $("#divAdd").collapse("show");
  resetCampi($(piatto).val());
}

//Modifica label e valore del pulsante a seconda del tipo di piatto
function modifyText(piatto) {
  $("label[for= 'nomePiatto']").html("Nome " + piatto);
  $("#add").val("Aggiungi " + piatto);
}

//Resetta i campi se si cambia il tipo di piatto dal radio button
function resetCampi(piatto) {
  if (piatto != "pizza" || piatto != "panino") {
    if (piatto != "primo") {
      $("#formaggio").prop("checked", false);
      $("#mod").prop("checked", false);
    }
    $("#nomePiatto").val("");
    $("#price").val("");
  }
  resetPrezzi();
}

function modifyPrice(){
  if($("#mod").prop("checked")){
    $("#new").val($("#prezzoTotale").text()*1);
    $("#nuovo").collapse("show");
  } else {
    $("#nuovo").collapse("hide");
    $("#new").val(0);
  }
}

function cambiaPagina() {
  window.location.href = "adminEntryPage.php";
}
