function checkEmail(str) {
  var char;
  var sum = 0;
  for(var i = 0; i < str.length; i++) {
    char = str.charAt(i);
    if(char === "@") {
      sum++;
    }
  }
  if(str.length != 0 && sum != 1) {
    $("#alert").show();
  } else {
    $("#alert").hide();
  }
}

function checkLength(str) {
  if(str.length != 0 && str.length < 8) {
    $("#lunghezza").show();
  } else {
    $("#lunghezza").hide();
  }
}

function checkChars(str) {
  var char;
  var char2;
  var sum = 0;
  var i = 0
  for(var i = 0; i < str.length; i++) {
    char = str.charAt(i);
    if(!Number.isNaN(parseInt(char))) {
      sum++;
    }
    if(char === char.toUpperCase()) {
      sum++;
    }
  }
  if(str.length != 0 && sum === 0) {
    $("#caratteri").show();
  } else {
    $("#caratteri").hide();
  }
}

function checkPassword(str1, str2) {
  if(str1.length != 0 && str2.length != 0 && str1 != str2) {
    $("#alert2").show();
  } else {
    $("#alert2").hide();
  }
}

function isEmpty() {
  return $("#nome").val() == ""
  || $("#cognome").val() == ""
  || $("#indizzo").val() == ""
  || $("#username").val() == ""
  || $("#email").val() == ""
  || $("#password").val() == ""
  || $("#confermapsw").val() == ""
  || $("#carta").val() == ""
  || $("#privacy").is(":not(:checked)");
}

function hasError() {
  return $("#alert").css("display") != "none"
  || $("#alert2").css("display") != "none"
  || $("#lunghezza").css("display") != "none"
  || $("#caratteri").css("display") != "none";
}

function loginRequest() {
   var usr = $("#username").val();
   var firstn = $("#nome").val();
   var lastn = $("#cognome").val();
   var addr = $("#indirizzo").val();
   var psswd = $("#password").val();
   var email = $("#email").val();
    var carta = $("#carta").val();
   //var dati = $("#form").serialize();
   var xhr = new XMLHttpRequest();
   xhr.onreadystatechange = function() {
      if ( xhr.readyState == 4 && xhr.status == 200 ){
        if(xhr.responseText != "New record created successfully") {
          $(".modal-title").html(xhr.responseText);
          $("#modcontent").html("Impossibile continuare la registrazione.");
          $("#success").collapse("hide");
          $("#wrong").collapse("show");
        } else {
          $(".modal-title").html("Operazione avvenuta con successo");
          $("#modcontent").html("Grazie per la registrazione");
          $("#success").collapse("show");
          $("#wrong").collapse("show");
        }
      }
   };
   xhr.open("POST","php/dbInsertion/insertUser.php", true );
   xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.send("username=" + usr + "&firstname=" + firstn + "&lastname=" + lastn + "&email=" + email + "&address=" + addr + "&password=" + psswd + "&carta=" + carta);
}

$(document).ready(function() {
    $("#email").focusout(function() {
       checkEmail($("#email").val());
    });

    $("#password").focusout(function() {
       var str = $("#password").val();
       checkLength(str);
       checkChars(str);
       checkPassword($("#password").val(), $("#confermapsw").val());
    });

    $("#confermapsw").focusout(function() {
       checkPassword($("#password").val(), $("#confermapsw").val());
    });

     $("#reg").click(function() {
       if(hasError() || isEmpty()) {
          $(".modal-title").html("Attenzione! Errori rilevati.");
          $("#modcontent").html("Impossibile continuare la registrazione.");
          $("#success").collapse("hide");
          $("#wrong").collapse("show");
       } else {
          loginRequest();
       }
     });
});
