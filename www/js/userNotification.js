function reloadPage( elem, div, user, baseUrl){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        if(xhr.responseText == "non ci sono ordini"){
        $(div).hide();
     } else {
       $(elem).html(xhr.responseText);
     }
   }
  };
  xhr.open("GET",baseUrl+"&user="+user, true );
  xhr.send();
}

function archivia(id){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        load();
     }
  };
  xhr.open("GET","php/userOrders.php?tipo=archivia&id="+id, true );
  xhr.send();
}

function load(){
  var user = $("#utente").text();
  baseUrl = "php/userOrders.php?tipo=";
  reloadPage( "#ordini1", "#inviati", user, baseUrl+"nonPronti");
  reloadPage( "#ordini2", "#inEsecuzione", user, baseUrl+"esecuzione");
  reloadPage( "#ordini3", "#pronti", user, baseUrl+"pronti");
}

$(document).ready(function() {
 load();
setInterval('load()',5000);

});
