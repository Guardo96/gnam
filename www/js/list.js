//variabile globale
var check;
var opt = 0;

function show(index) {
  $("#coll" + index).collapse("toggle");
  if($("#btnShow" + index).html() == '<i class="fa fa-angle-up"></i>') {
    $("#btnShow" + index).html('<i class="fa fa-angle-down"></i>');
  } else {
    $("#btnShow" + index).html('<i class="fa fa-angle-up"></i>');
  }
}

function checkButton(am, str) {
  if(am <= 0) {
    $(str).css("opacity", "0.5");
    $(str).css("cursor", "not-allowed");
  } else {
    $(str).css("opacity","1");
    $(str).css("cursor", "pointer");
  }
}

function add(index) {
  var amount = $("#num" + index).text();
  amount++;
  setSessionStorage(index, amount);
  $("#num" + index).text(amount);
  checkButton(amount, "#less" + index);
}

function remove(index) {
  var amount = $("#num" + index).text();
  if($("#less" + index).css("cursor") != "not-allowed") {
    amount--;
    setSessionStorage(index, amount);
    $("#num" + index).text(amount);
    checkButton(amount, "#less" + index);
  }
}

function setSessionStorage(index, amount){
  var nomeP = $("#name"+index).html();
  if(amount == 0){
    sessionStorage.removeItem(nomeP);
  } else {
    if(sessionStorage.getItem("tipoPiatto") == "primo"){
      var form = $("#aggiuntaform"+index).text();
      var dato = sessionStorage.getItem("tipoPiatto") + "_" + amount + "_" + form;
    } else {
      var dato = sessionStorage.getItem("tipoPiatto") + "_" + amount;
    }
    sessionStorage.setItem(nomeP, dato);
  }
  $("#num" + index).text(amount);
  checkButton(amount, "#less" + index);
}

function dividiStringhe(stringa){
  var arrayString = stringa.split("_");
  return arrayString;
}

function checkProdottiOrd(){
  var nProdotti = $("#numeroProdotti").text();
  for(var i = 0; i < nProdotti; i++){
    var nomeP = $("#name"+i).html();
    var ordine = sessionStorage.getItem(nomeP);
    if(ordine != null){
       var dati = dividiStringhe(ordine);
         $("#num" + i).text(dati[1]);
    }
  }
}

function selectSecondi(tipo){
  var user = $("#SessionUser").text();
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
       $("#listaPiatti").html(xhr.responseText);
       checkProdottiOrd();
     }
  };
  xhr.open("GET","php/cambiaSecondi.php?user="+user+"&tipo="+tipo, true );
  xhr.send();
}


$(document).ready(function() {

  if($("h1").html() == "I nostri menù") {
    sessionStorage.setItem("tipoPiatto", "menu");
  }
  checkProdottiOrd();
  });
