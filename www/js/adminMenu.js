
var prezzoTot = 0;
var prezzoScontato = 0;
var prezzoPrimo = 0;
var prezzoSecondo = 0;
var prezzoContorno = 0;
var prezzoStuzzichino = 0;
var prezzoDessert = 0;
var prezzoBibita = 0;
var tipo;

function changeTotalPrice(pr){
  switch(tipo){
    case "primo":
      prezzoTot -= prezzoPrimo*1;
      prezzoPrimo = pr;
      break;
    case "secondo":
      prezzoTot -= prezzoSecondo*1;
      prezzoSecondo = pr;
      break;
    case "contorno":
     prezzoTot -= prezzoContorno*1;
      prezzoContorno = pr;
      break;
    case "stuzzichino":
      prezzoTot -= prezzoStuzzichino*1;
      prezzoStuzzichino = pr;
      break;
    case "dessert":
      prezzoTot -= prezzoDessert*1;
      prezzoDessert = pr;
      break;
    case "bibita":
      prezzoTot -= prezzoBibita*1;
      prezzoBibita = pr;
      break;
    default :
      break;
  }
  prezzoTot += pr*1;
  prezzoTot = new Number(prezzoTot).toFixed(2);
  prezzoScontato = prezzoTot;
  $("#prTotale").html("Prezzo totale prodotti: " + prezzoTot + "€");
  $("#prScontato").html("Prezzo menu: " + prezzoTot + "€");
}

function priceRq(val, elem){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
       price = xhr.responseText
       changeTotalPrice(price);
     }
  };
  xhr.open("GET","../php/priceQuery.php?val=" + val + "&elem=" + elem, true );
  xhr.send();
};

function insertMenu() {
  var nome = $("#title").val();
  var prezzo = prezzoScontato;
  var stuzzichino = $("#stuzzichino").val();
  var primo = $("#primo").val();
  var secondo = $("#secondo").val();
  var contorno = $("#contorno").val();
  var dessert = $("#dessert").val();
  var bibita = $("#bibita").val();
  var url = "../php/dbInsertion/insertMenu.php?nome="+nome+"&username=admin&stuzzichino="+stuzzichino+"&primo="+primo+"&primo="+primo+"&secondo="+secondo+"&contorno="+contorno+"&dessert="+dessert+"&bibita="+bibita+"&prezzo="+prezzo;

  if(checkFormMenu(nome)) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
       if ( xhr.readyState == 4 && xhr.status == 200 ){
         $(".modal-title").html(xhr.responseText);
         $("#modcontent").html("Inserimento " + nome + " riuscito");
         $("#wrong > button").html("Aggiungi altro");
         $("#success").collapse("show");
         $("#wrong").collapse("show");
       }
    };
    xhr.open("GET",url, true );
    xhr.send();
  } else {
    var str = "";
    if(prezzo == 0)
    {
      str = "ATTENZIONE!: devi scegliere almeno un piatto";
    }
    if(nome == "")
    {
      str = "ATTENZIONE!: dare un nome al menù";
      if(prezzo == 0) {
        str = "Impossibile proseguire";
      }
    }
    $(".modal-title").html(str);
    $("#modcontent").html("Inserimento non riuscito");
    $("#success").collapse("hide");
    $("#wrong").collapse("show");
  }
};

// Ritorna vero se il titolo non è vuoto e se almeno un piatto è stato scelto
function checkFormMenu(titolo) {
  return (titolo != "" && prezzoScontato != 0);
}

function cambiaPagina() {
  window.location.href = "adminEntryPage.php";
}

$(document).ready(function() {

$("#mod").click(function() {
  $("#nuovo").collapse("toggle");
});

$("#mod").change(function(){
  prezzoScontato = prezzoTot;
  $("#new").val("");
  $("#prScontato").html("Prezzo menu: " + prezzoScontato + "€");

});

$("#new").change(function(){
  var sconto = $("#new").val();
  temp = prezzoTot;
  prezzoScontato = ((temp*100) - (sconto*100))/100;
  $("#prScontato").html("Prezzo menu: " + prezzoScontato + "€");

});

$("#primo").change( function() {
  var elemento = $("#primo").val();
  var value ="primo";
  tipo = value;
  priceRq(value,elemento);
});

$("#secondo").change( function() {
  var elemento = $("#secondo").val();
  var value ="secondo";
  tipo = value;
  priceRq(value,elemento);
});

$("#contorno").change( function() {
  var elemento = $("#contorno").val();
  var value ="vario";
  tipo= "contorno";
  priceRq(value,elemento);
});

$("#stuzzichino").change( function() {
  var elemento = $("#stuzzichino").val();
  var value ="vario";
  tipo = "stuzzichino";
  priceRq(value,elemento);
});

$("#dessert").change( function() {
  var elemento = $("#dessert").val();
  var value ="vario";
  tipo = "dessert";
  priceRq(value,elemento);
});

$("#bibita").change( function() {
  var elemento = $("#bibita").val();
  var value ="vario";
  tipo = "bibita";
  priceRq(value,elemento);
});

});
