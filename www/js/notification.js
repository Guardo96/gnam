

function reloadPage( stat, elem, user, baseUrl){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        $(elem).html(xhr.responseText);
     }
  };
  xhr.open("GET",baseUrl+stat+"&user="+user+"&nuovostato=" + stat, true );
  xhr.send();

}

function cambiaStato(id, stato){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        load();
     }
  };
  xhr.open("GET","../php/selectOrders.php?action=cambiastato&nuovoStato="+stato+"&id="+id, true );
  xhr.send();
}




function load(){
  var user = $("#utente").text();
  baseUrl = "../php/selectOrders.php?action=";
  reloadPage("daFare", "#daEsec", user, baseUrl);
  reloadPage("esecuzione", "#inEsec",user, baseUrl);
}

$(document).ready(function() {
 load();

setInterval('load()',5000);

});
