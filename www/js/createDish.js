  var tipo;
  var typeElem;
  var username;
  var check;

// Funzione usata dal pulsante Aggiungi ingrediente per i secondi
function addIngr(file) {
  var num = $("#numIng").text();
  var prev;
  if(num == 0) {
    prev = "#base2s";
  } else {
    prev = "#ingrediente" + opt;
  }
  opt = num;
  num++;
  ingredientRequest(check, "#ingrediente" + opt, file);
  var isFull = (prev == "#base2s") ? ($("#base1s").val() != "" && $(prev).val() != "") : ($(prev).val() != "");
  if(isFull) {
    if(opt == 0) {
      $("#btnRem").css("display", "block");
    }
    $("#ing" + opt).collapse("show");
    $("#numIng").text(num);
  } else {
    opt--;
  }
  if (opt == 5) {
    $("#btnAdd").css("display", "none");
  }
}

function resetIngr(){
  for(i = 0; i < 6;i++) {
      $("#ingrediente"+i).html("");
      $("#ing"+i).collapse("hide");
  }
  $("#numIng").text("0");
  $("#btnAdd").css("display", "block");
  $("#btnRem").css("display", "none");
  $("#mod").prop("checked", false);
}

function removeIngr() {
  var num = $("#numIng").text();
  if(num == 6) {
    $("#btnAdd").css("display", "block");
  }
  if(num > 0) {
    num--;
    opt--;
    $("#ingrediente" + num).html("");
    $("#ing" + num).collapse("hide");
    $("#numIng").text(num);
    typeElem ="ingrediente"+num;
    changeTotalPrice(0);
  }
  if(num == 0) {
    $("#btnRem").css("display", "none");
  }
}

function resetPrezzi(){
  $("#new").val(0);
  $("#price").val("");
  $("#priceInfo").text("Prezzo: ");
  $("#priceInfoS").text("Prezzo: ");
  $("#prezzoTotale").text("0");
  $("#prezzoBase1").text("0");
  $("#prezzoBase2").text("0");
  $("#prezzoIngrediente0").text("0");
  $("#prezzoIngrediente1").text("0");
  $("#prezzoIngrediente2").text("0");
  $("#prezzoIngrediente3").text("0");
  $("#prezzoIngrediente4").text("0");
  $("#prezzoIngrediente5").text("0");
  $("#nuovo").collapse("hide");
}

function reset(piatto){
  $("#nomePiatto").val("");
  if (piatto == "#secondo" || piatto == "secondo"){
    $("#base1s").val("");
    $("#base2s").val("");
  } else if(piatto == "#primo" || piatto == "primo"){
    $("#base1").val("");
    $("#base2").val("");
  }
}

function sceltaBase(click, file) {
  check = click;
  $("#base1s").val("");
  $("#base2s").val("");
  tipo = check;
  if(check == "panino") {
    $("label[for='base1s']").html("Scegli base per panino:");
    $("label[for='base2s']").html("Scegli salsa:");
  } else {
    $("label[for='base1s']").html("Scegli farina per pizza:");
    $("label[for='base2s']").html("Scegli base per pizza:");
  }
  $("#divBSecondo").collapse("show");
  $(".modificaPrezzo").collapse("show");
  ingredientRequest(check, "#base1s", file);
  ingredientRequest(check, "#base2s", file);
  resetPrezzi();
  resetIngr();
}

function ingredientRequest(value, sezione, file) {
  if(sezione == '#base1'|| sezione == '#base1s'){
    if(value == 'pizza'){
      var url = file +"?value=" + value + "&sez=farina";
    } else if(value == 'panino'){
      var url = file + "?value=" + value + "&sez=pane";
    } else if(value == 'primo') {
      var url = file +"?value=" + value + "&sez=pasta";
    }
  } else if(sezione == '#base2' || sezione == '#base2s') {
    if(value == 'pizza'){
      var url = file + "?value=" + value + "&sez=base_pizza";
    } else if(value == 'panino'){
      var url = file + "?value=" + value + "&sez=salsa";
    } else if(value == 'primo') {
      var url = file +"?value=" + value + "&sez=sugo";
    }
  } else {
      var url = file + "?value=" + value + "&sez=ingr";
  }
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
       if ( xhr.readyState == 4 && xhr.status == 200 ){
         $(sezione).html("<option></option>" + xhr.responseText);
       }
    };
    xhr.open("GET",url, true );
    xhr.send();
}

function checkValue(piatto){
  if($("#nomePiatto").val() == ""){
     $(".modal-title").html("ATTENZIONE! Dare un nome al piatto");
     return true;
  }
  if(piatto == "secondo" || piatto == "#secondo"){
    if(check == undefined) {
      $(".modal-title").html("ATTENZIONE! Scegliere il tipo di secondo da inserire");
      return true;
    }
    if(  $("#base1s").val() == "" || $("#base1s").val() == null || $("#base1s").val() == "" || $("#base1s").val() == null){
      if(check == "panino"){
        $(".modal-title").html("ATTENZIONE! Scegliere il tipo di pane e la salsa ");
        return true;
      }
      if(check == "pizza"){
        $(".modal-title").html("ATTENZIONE! Scegliere il tipo di farina e la base per la pizza ");
        return true;
      }
    }
    if($("#ingrediente0").val() == "" || $("#ingrediente0").val() == null){
      if(check == "panino"){
        $(".modal-title").html("ATTENZIONE! Aggiungere almeno un elemento nel panino");
        return true;
      }
    }
  }
  if(piatto == "primo" || piatto == "#primo"){
    if(  $("#base1").val() == "" || $("#base1").val() == null || $("#base2").val() == "" || $("#base2").val() == null){
      $(".modal-title").html("ATTENZIONE! Scegliere il tipo di pasta e il sugo ");
      return true;
    }
  }
  if(piatto != "#primo" && piatto != "primo" && piatto != "#secondo" && piatto != "secondo") {
    if($("#price").val() == ""){
       $(".modal-title").html("ATTENZIONE! Dare un prezzo al piatto");
       return true;
    }
  }
  return false;
}

function insertDish(){
  var piatto = sessionStorage.getItem("tipoPiatto");
  if(checkValue(piatto)){
    $("#modcontent").html("Inserimento non riuscito");
    $("#wrong > button").html("Torna al form");
    $("#success").collapse("hide");
    $("#wrong").collapse("show");
  }
  var nome = $("#nomePiatto").val();
  var prezzo = $("#price").val();
  var base1;
  var base2;
  var url;
  if(piatto == "#primo" || piatto == "primo")
  {
     base1 = $("#base1").val();
     base2 = $("#base2").val();
     var p = $("#priceInfo").text().split(" ");
     prezzo = p[1];
     var formaggio;
    if(document.getElementById("formaggio").checked)
    {
      formaggio = "Y";
    }else{
      formaggio = "N";
    }
    if(piatto == "primo"){
      var baseUrl = "php/dbInsertion/insertFood.php?username="+username;
    } else {
      if($("#mod").prop("checked") && $("#new").val() != 0 && $("#new").val() != null){
        prezzo = $("#new").val();
      }
      var baseUrl = "../php/dbInsertion/insertFood.php?username=admin";
    }
    url = baseUrl + "&nome="+nome+"&tipo=primo&base="+base1+"&base2="+base2+"&prezzo="+prezzo+"&ing1=null&ing2=null&ing3=null&ing4=null&ing5=null&ing6=null&aggiunta_formaggio="+formaggio;

  }else if(piatto == "#secondo" || piatto == "secondo")
  {
     var base1 = $("#base1s").val();
     var base2 = $("#base2s").val();
     var ing1 = $("#ingrediente0").val();
     var ing2 = $("#ingrediente1").val();
     var ing3 = $("#ingrediente2").val();
     var ing4 = $("#ingrediente3").val();
     var ing5 = $("#ingrediente4").val();
     var ing6 = $("#ingrediente5").val();
     var p = $("#priceInfoS").text().split(" ");
     prezzo = p[1];
     if(piatto == "secondo"){
       var baseUrl = "php/dbInsertion/insertFood.php?username="+username;
     } else {
       if($("#mod").prop("checked") && $("#new").val() != 0 && $("#new").val() != null){
         prezzo = $("#new").val();
       }
       var baseUrl = "../php/dbInsertion/insertFood.php?username=admin";
     }
     url = baseUrl + "&nome="+nome+"&tipo="+tipo+"&base="+base1+"&base2="+base2+"&prezzo="+prezzo+"&ing1="+ing1+"&ing2="+ing2+"&ing3="+ing3+"&ing4="+ing4+"&ing5="+ing5+"&ing6="+ing6+"&aggiunta_formaggio=N";
  } else  {
    if(piatto == "#stuzzichino")
    {
      tipo = "stuzzichino";
    }else if(piatto == "#contorno")
    {
      tipo = "contorno";
    }else if(piatto == "#dessert")
    {
      tipo = "dessert";
    }else if(piatto == "#bibita")
    {
      tipo = "bibita";
    }
    url =  "../php/dbInsertion/insertVario.php?nome="+nome+"&tipo="+tipo+"&prezzo="+prezzo;
  }
  if(!checkValue(piatto)) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
       if ( xhr.readyState == 4 && xhr.status == 200 ){
         reset(piatto);
         resetIngr();
         if(username != "admin"){
           $(".modal-title").html(xhr.responseText);
           $("#modcontent").html("Inserimento " + name + " riuscito");
           $("#wrong > button").html("continua");
           $("#wrong").collapse("show");
           $("#success").collapse("hide");
           reloadPage();
         } else {
           $(".modal-title").html(xhr.responseText);
           $("#modcontent").html("Inserimento " + name + " riuscito");
           $("#wrong > button").html("Aggiungi altro");
           $("#success").collapse("show");
           $("#wrong").collapse("show");
           resetPrezzi();
         }
       }
    };
    xhr.open("GET",url, true );
    xhr.send();
  }
}

function reloadPage(){
  var i = $("numeroProdotti").text();
  var cat =  sessionStorage.getItem("tipoPiatto");
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
       $("#listaPiatti").html(xhr.responseText);
     }
  };
  xhr.open("GET","php/createFoodList.php?request=reload&user="+username+"&categoria="+cat, true );
  xhr.send();
}

function changeTotalPrice(pr){
      var prezzoPiatto = $("#prezzoTotale").text();
      var ingrediente0 = $("#prezzoIngrediente0").text();
      var ingrediente1 = $("#prezzoIngrediente1").text();
      var ingrediente2 = $("#prezzoIngrediente2").text();
      var ingrediente3 = $("#prezzoIngrediente3").text();
      var ingrediente4 = $("#prezzoIngrediente4").text();
      var ingrediente5 = $("#prezzoIngrediente5").text();
      var base1 = $("#prezzoBase1").text();
      var base2 = $("#prezzoBase2").text();

  switch(typeElem){
    case "base1":
      prezzoPiatto -= base1*1;
      base1= pr;
      break;
    case "base2":
      prezzoPiatto -= base2*1;
      base2 = pr;
      break;
    case "ingrediente0":
      prezzoPiatto -= ingrediente0*1;
      ingrediente0 = pr;
      break;
    case "ingrediente1":
      prezzoPiatto -= ingrediente1*1;
      ingrediente1 = pr;
      break;
    case "ingrediente2":
      prezzoPiatto -= ingrediente2*1;
      ingrediente2 = pr;
      break;
    case "ingrediente3":
      prezzoPiatto -= ingrediente3*1;
      ingrediente3 = pr;
      break;
   case "ingrediente4":
      prezzoPiatto -= ingrediente4*1;
      ingrediente4 = pr;
      break;
    case "ingrediente5":
      prezzoPiatto -= ingrediente5*1;
      ingrediente5 = pr*1;
      break;
    default :
      break;
  }
  prezzoPiatto += pr*1;
  $("#prezzoTotale").text(prezzoPiatto);
  $("#prezzoIngrediente0").text(ingrediente0);
  $("#prezzoIngrediente1").text(ingrediente1);
  $("#prezzoIngrediente2").text(ingrediente2);
  $("#prezzoIngrediente3").text(ingrediente3);
  $("#prezzoIngrediente4").text(ingrediente4);
  $("#prezzoIngrediente5").text(ingrediente5);
  $("#prezzoBase1").text(base1);
  $("#prezzoBase2").text(base2);
  if(sessionStorage.getItem("tipoPiatto") == "#primo" || sessionStorage.getItem("tipoPiatto") == "primo"){
  $("#priceInfo").html("Prezzo: "+prezzoPiatto+" €");
  } else {
    $("#priceInfoS").html("Prezzo: "+prezzoPiatto+" €");
  }
}

function priceRq(val, elem){
  var xhr = new XMLHttpRequest();
  if(username == "admin"){
    var url = "../php/priceQuery.php?val=" + val + "&elem=" + elem;
  } else {
    var url = "php/priceQuery.php?val=" + val + "&elem=" + elem;
  }
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
       price = xhr.responseText;
       changeTotalPrice(price);
     }
  };
  xhr.open("GET",url, true );
  xhr.send();
};

$(document).ready(function() {
  username = $("#user").text();
  if($("h1").html() == "primo") {
    sessionStorage.setItem("tipoPiatto", "primo");
    ingredientRequest("primo", "#base1", "php/IngrQuery.php");
    ingredientRequest("primo", "#base2", "php/IngrQuery.php");
  };
  if($("h1").html() == "secondo") {
    sessionStorage.setItem("tipoPiatto", "secondo");
  };
  if($("h1").html() == "contorno") {
    sessionStorage.setItem("tipoPiatto", "contorno");
  };
  if($("h1").html() == "stuzzichino") {
    sessionStorage.setItem("tipoPiatto", "stuzzichino");
  };
  if($("h1").html() == "dessert") {
    sessionStorage.setItem("tipoPiatto", "dessert");
  };
  if($("h1").html() == "bibita") {
    sessionStorage.setItem("tipoPiatto", "bibita");
  };

  $("#base1").change( function() {
    var elem = $("#base1").val();
    typeElem = "base1";
    priceRq("elemento", elem)
  });

  $("#base2").change( function() {
    var elem = $("#base2").val();
    typeElem = "base2";
    priceRq("elemento", elem)
  });

  $("#base1s").change( function() {
    var elem = $("#base1s").val();
    typeElem = "base1";
    priceRq("elemento", elem)
  });

  $("#base2s").change( function() {
    var elem = $("#base2s").val();
    typeElem = "base2";
    priceRq("elemento", elem)
  });

  $("#ingrediente0").change( function() {
    var elem = $("#ingrediente0").val();
    typeElem = "ingrediente0";
    priceRq("elemento", elem)
  });

  $("#ingrediente1").change( function() {
    var elem = $("#ingrediente1").val();
    typeElem = "ingrediente1";
    priceRq("elemento", elem)
  });

  $("#ingrediente2").change( function() {
    var elem = $("#ingrediente2").val();
    typeElem = "ingrediente2";
    priceRq("elemento", elem)
  });

  $("#ingrediente3").change( function() {
    var elem = $("#ingrediente3").val();
    typeElem = "ingrediente3";
    priceRq("elemento", elem)
  });

  $("#ingrediente4").change( function() {
    var elem = $("#ingrediente4").val();
    typeElem = "ingrediente4";
    priceRq("elemento", elem)
  });

  $("#ingrediente5").change( function() {
    var elem = $("#ingrediente5").val();
    typeElem = "ingrediente5";
    priceRq("elemento", elem)
  });
});
