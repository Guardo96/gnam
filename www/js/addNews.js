function postnews() {
    var titolo = $("#titolo").val();
    var testo = $("#testo").val();
    if(titolo != "" && testo != "") {
    var xhr = new XMLHttpRequest;
      xhr.onreadystatechange = function() {
          if (xhr.readyState == 4 && xhr.status == 200) {
              $(".modal-title").html("Inserita nuova notizia!");
              $("#modcontent").html("Inserimento notizia riuscito");
              $("#wrong >button").html("Aggiungi altro");
              $("#success").collapse("show");
              $("#wrong").collapse("show");
              reset();
          }
      };
      xhr.open("POST", "../php/dbInsertion/insertNews.php", true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.send("titolo=" + titolo + "&testo=" + testo);
    } else {
    var str = checkForm(titolo, testo);
    $(".modal-title").html(str);
    $("#modcontent").html("Inserimento non riuscito");
    $("#wrong > button").html("Torna al form");
    $("#success").collapse("hide");
    $("#wrong").collapse("show");
  }
}

function checkForm(titolo, testo) {
  var str = "";
  if(titolo == "") {
    str = "ATTENZIONE! Campo TITOLO vuoto";
  }
  if(testo == "") {
    str = "ATTENZIONE! Campo TESTO vuoto";
  }
  if(titolo == "" && testo == "") {
    str = "ATTENZIONE! Riempire tutti i campi"
  }
  return str;
}
function reset() {
  $("#titolo").val("");
  $("#testo").val("");
}

function cambiaPagina() {
  window.location.href = "adminEntryPage.php";
}
