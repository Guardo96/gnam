
var nProdotti = 0;

function inviaOrdine(){
  user = $("#username").text();
  var tot = $("#prTotale").text().split(" ");
  url= "php/dbInsertion/insertOrder.php?code=ordine&username=" + user + "&prezzo=" + tot[2] + "&stato=inviato";
  var xhr = new XMLHttpRequest
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        id_ordine = xhr.responseText;
        visualizzaIndirizzo();
        insertProdotti(id_ordine);
     }
  };
  xhr.open("GET",url, true );
  xhr.send();
}

function insertProdotti(id_prodotto){
  url ="php/dbInsertion/insertOrder.php?code=prodotto&id_ordine="+id_ordine;
  for(var i =  0; i < sessionStorage.length; i++){
    var prodotto = sessionStorage.key(i);
    if(prodotto != "tipoPiatto"){
      var dati = dividiStringhe(sessionStorage.getItem(prodotto));
      url += "&prod="+ prodotto + "&tipo=" + dati[0] + "&qta=" + dati[1];
      var xhr = new XMLHttpRequest
      xhr.onreadystatechange = function() {
        if ( xhr.readyState == 4 && xhr.status == 200 ){
        }
      };
      xhr.open("GET",url, true );
      xhr.send();
    }
  }
  var nprod= nProdotti;
  for(var i =  0; i < nprod; i++){
       rimuovi("prod"+i);
  }
}

function visualizzaIndirizzo() {
  var user = $("#username").text();
  var tot = $("#prTotale").text().split(" ");
  var strtot = tot[2] + "€ sono stati prelevati dalla tua carta... Presto potrai mangiare a volontà!";
  var url = "php/addressQuery.php?user=" + user + "&type=select&val=0";
  var checkbox = '</div><div class="custom-control custom-checkbox">' +
                  '<input type="checkbox" id="cambiaind" name="cambia" class="custom-control-input"/>' +
                  '<label class="custom-control-label" for="cambia" onclick="mostra()">Cambia indirizzo</label></div>' +
                  '<div class="collapse" id="divnuovoInd"><label for="nuovoInd">Nuovo indirizzo:</label>' +
                  '<input type="text" id="nuovoInd" name="nome" class="form-control input-sm"/></div>' +
                  '<div class="divbtn collapse" id="btnind"><button class="btn btn-outline-primary" id="cambiaind"' +
                  'data-toggle="modal" data-target="#modal" onclick="cambiaIndirizzo()">Cambia</button>';
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if(xhr.readyState == 4 && xhr.status == 200) {
      $(".modal-title").html("Ordine pronto per essere inviato. Vuoi cambiare indirizzo?");
      $("#modcontent").html(strtot + '<div id="addr">Indirizzo attuale: ' + xhr.responseText + checkbox);
      $("#success").collapse("hide");
      $("#wrong").collapse("show");
    }
  };
  xhr.open("GET", url, true);
  xhr.send();
}


function cambiaIndirizzo() {
  var user = $("#username").text();
  var val = $("#nuovoInd").val();
  alert(val);
  var url = "php/addressQuery.php?user=" + user + "&type=update&val=" + val;
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    alert("ciaoooooo");
    if(xhr.readyState == 4 && xhr.status == 200) {
    }
  };
  xhr.open("GET", url, true);
  xhr.send();
}

function cambiaPagina() {
  window.location.href = "mainMenu.php";
}

function mostra() {
  $("#divnuovoInd").collapse("show");
  $("#btnind").collapse("show");
}

function cambiaFormaggio(prodotto, qta){
  var input = document.getElementById("qtaForm"+prodotto);
  input.setAttribute("max",qta*1);
  if($("#qtaForm"+prodotto).val() > qta){
      $("#qtaForm"+prodotto).val(qta);
  }
}

function calcolaPrezzo(){
  var prezzoTot=0;
  for(i=0; i < nProdotti; i++){
    var prezzo = $("#prezzoprod"+i).text().split(" ");
    prezzoTot += prezzo[2]*1;
  }
  $("#prTotale").text("Prezzo Ordine: " + prezzoTot + " €");
}

function cambiaQuantita(prodotto){
    var oldPrezzo = $("#prezzo"+prodotto).text().split(" ");
    var name = $("#nome"+prodotto).text();
    var qta = $("#numQta"+prodotto).val();
    var dati = dividiStringhe(sessionStorage.getItem(name));
    dati[1] = qta;
    if(dati[0] == "primo" && dati[2] == 'Y'){
      cambiaFormaggio(prodotto, qta);
      sessionStorage.setItem(name,dati[0]+"_"+dati[1]+"_"+dati[2]);
    } else{
      sessionStorage.setItem(name,dati[0]+"_"+dati[1]);      
    }
    var costo = $("#prezzouni"+prodotto).text().split(" ");
    var prezzo = (((qta*10)*(costo[2]*10))/100);
    $("#prezzo"+prodotto).text("Prezzo totale: " + prezzo + " €");
    var tot = $("#prTotale").text().split(" ");
    newTot = (((tot[2]*1)*100)-((oldPrezzo[2]*1)*100) + (prezzo*1)*100)/100;
    newTot = new Number(newTot).toFixed(2);
    $("#prTotale").text("Prezzo Ordine: " +  newTot + " €");
}

function rimuovi(prodotto) {
  var name = $("#nome"+prodotto).text();
  sessionStorage.removeItem(name);
  nProdotti--;
  var prezzo = $("#prezzo"+prodotto).text().split(" ");
  var tot = $("#prTotale").text().split(" ");
  var newTot = ((tot[2]*10)-(prezzo[2]*10))/10;
  newTot = new Number(newTot).toFixed(2);
  $("#prTotale").text("Prezzo Ordine: " +  newTot + " €");
  $("#" + prodotto).html("");
  $("#" + prodotto).hide();
}

function controllaTipo(prodotto, piatto) {
  if(piatto != "primo") {
    $("#" + prodotto + " .formaggio").hide();
  }
}

function dividiStringhe(stringa){
  var arrayString = stringa.split("_");
  return arrayString;
}

function incrementCount(prodotto, dati){
  var tipo = dati[0];
  if(tipo == "menu"){
     ajaxMenuRq(prodotto, dati, nProdotti++);
  } else if(tipo == "bibita") {
     ajaxBibiteRq(prodotto, dati, nProdotti++);
  } else {
     ajaxProdottoRq(prodotto, dati, nProdotti++);
  }
}

function ajaxMenuRq(prodotto, dati, n_menu){

  var xhr = new XMLHttpRequest
  xhr.onreadystatechange = function() {
     if ( xhr.readyState == 4 && xhr.status == 200 ){
        $("#menu").append(xhr.responseText);
     }
  };
  xhr.open("GET","php/createCart.php?prodotto="+prodotto+"&tipo="+dati[0]+"&quantita="+dati[1]+"&numero="+n_menu, false );
  xhr.send();
}

function ajaxProdottoRq(prodotto, dati, n_prodotti){
  var xhr1 = new XMLHttpRequest
  xhr1.onreadystatechange = function() {
     if ( xhr1.readyState == 4 && xhr1.status == 200 ){
        $("#prod").append(xhr1.responseText);
     }
  };
  xhr1.open("GET","php/createCart.php?prodotto="+prodotto+"&tipo="+dati[0]+"&quantita="+dati[1]+"&numero="+n_prodotti, false );
  xhr1.send();
}

function ajaxBibiteRq(prodotto, dati, n_prodotti){

  var xhr2 = new XMLHttpRequest
  xhr2.onreadystatechange = function() {
     if ( xhr2.readyState == 4 && xhr2.status == 200 ){
        $("#bibite").append(xhr2.responseText);
     }
  };
  xhr2.open("GET","php/createCart.php?prodotto="+prodotto+"&tipo="+dati[0]+"&quantita="+dati[1]+"&numero="+n_prodotti, false );
  xhr2.send();
}

function createPage(){
  nProdotti = 0;
  for(var i =  0; i < sessionStorage.length; i++){
    var prodotto = sessionStorage.key(i);
    if(prodotto != "tipoPiatto"){
       var dati = dividiStringhe(sessionStorage.getItem(prodotto));
       incrementCount(prodotto, dati);
    }
  }
}

$(document).ready(function() {
  createPage();
  calcolaPrezzo();
});
