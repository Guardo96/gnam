<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Lista prodotti</title>
    <?php include('../include/head.php'); ?>
    <link href="../css/mainStyle.css" rel="stylesheet"/>
    <link href="../css/account.css" rel="stylesheet"/>
    <script type="text/javascript" src="../js/list.js"></script>
    <script type="text/javascript" src="../js/everything.js"></script>
    <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
<?php include('../include/adminNavbar.php'); ?>
<?php $index = 0; ?>
<h1>Lista prodotti</h1>
<div id="piatti" class="divgrad">
    <h2>Lista piatti</h2>
    <?php
    $piatti = array("stuzzichino", "primo", "pizza", "panino", "contorno", "dessert", "bibita");
    for($i = 0; $i < 7; $i++) {
        if($i > 0 && $i < 4) {
            listtable("piatto", $piatti[$i]);
        } else {
            listtable("vario", $piatti[$i]);
        }
    }
    ?>
</div>
<div id="menu" class="divgrad">
    <h2>Lista menù</h2>
    <div id="divlist">
        <?php listtable("menu",""); ?>
    </div>
    <div id="elemento" class="divgrad">
        <h2>Lista ingredienti</h2>
        <?php
        $ingr = array("pasta", "sugo", "farina", "base_pizza", "pane", "salsa", "ingrediente");
        for($i = 0; $i < 7; $i++) {
            listtable("elemento", $ingr[$i]);
        }
        ?>
    </div>
    <div id="news" class="divgrad">
        <h2>Lista news</h2>
        <div id="divnews">
            <?php listtable("news",""); ?>
        </div>
</body>
</html>

<?php
function listtable($table, $tipo) {
    global $index;
    try {
        $database = include("../php/db/dbconfig.php");
        $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($tipo == "") {
            $statement = $conn->prepare("SELECT * FROM $table");
            $statement->execute();
        } else {
            $statement = $conn->prepare("SELECT * FROM $table WHERE tipo = '$tipo'");
            $statement->execute();
        }
        $count = $statement->rowCount();
        if($table != "menu" && $table != "news") {
            echo setHeader($tipo, $index, $table);
        }
        if($count == 0) {
            echo scegliParola($tipo, $table);
            $index++;
        } else {
            while ($row = $statement->fetch()) {
                if($table != "menu" && $table != "news") {
                    echo '<div class="elemList" id="per' . $index . '">';
                    echo '<div id="' . $index .'" >';
                }
                setText($row, $table, $index, $tipo);
                $fmod = 'onclick="update(' . "'" . $row[0] . "', '" . $index . "', '" . $table . "'" . ')"';
                $modifica = setModifica($index, $fmod, $table, $row[0]);
                $fmclick = 'onclick="modifica(' . "'" . $index . "'" . ')"';
                $func = 'onclick="rimuovi(' . "'" . $row[0] . "', '" . $index . "', '" . $tipo . "', '" . $table . "')" . '"';
                if($table != "menu" && $table != "news") {
                    echo '</div>
                <div class="divbtn">
                  <button id="rim' . $index . '" class="btn btn-outline-primary" ' . $func . '>Rimuovi</button>
                  <button id="mod' . $index . '" class="btn btn-outline-primary" ' . $fmclick . '>Modifica prezzo</button>
                </div>' . '</div>' . $modifica;
                } else {
                    $button = '<button id="mod' . $index . '" class="btn btn-outline-primary" ' . $fmclick . '>Modifica prezzo</button>';
                    if($table == "news") {
                        $button = '<button id="mod' . $index . '" class="btn btn-outline-primary" ' . $fmclick . '>Modifica testo</button>';
                    }
                    echo '</div>
                <div class="divbtn">
                  <button id="rim' . $index . '" class="btn btn-outline-primary" ' . $func . '>Rimuovi</button>'
                        . $button .
                        '</div>
              </div>' . $modifica . '</div>';
                }
                $index++;
            }
            if ($count != 0) {
                echo '</div>';
            }
        }
    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

function setText($row, $table, $index, $tipo) {
    $i = 0;
    foreach ($row as $key => $value) {
        if ($i % 2 == 0) {
            $str = "<span>$key:</span> $value <br/>";
            if($table == "menu" || $table == "news") {
                if ($key == "id") {
                    echo setHeader($row[1], $index, $table);
                    echo '<div class="elemList per' . $index . '">
                <div id="' . $index .'" >';
                }
                if ($key == "nome" || $key == "titolo") {
                    $str = "";
                }
                if (checkMenu($value)) {
                    $str = "<span>$key:</span> <br/>";
                }
            } else {
                $keyText = checkType($tipo, $key);
                $str = "<span>$keyText:</span> $value <br/>";
                if((($tipo == "pizza" || $tipo == "panino") && $key == "aggiunta_formaggio") || $value == "null" ||
                    ($table == "elemento" && $tipo != "ingrediente" && ($key == "per_pizza" || $key == "per_panino")) || $key == "tipo") {
                    $str = "";
                }
            }
            if($key == "testo") {
              $str = '<div id="testo' . $index . '"><span>' . $key . ':</span> ' . $value . '<br/></div>';
            }
            if($key == "sconto") {
                $str = '<div id="sconto' . $index . '"><span>' . $key . ':</span> ' . $value . '<br/></div>';
            }
            if($key == "prezzo" || $key == "add_price") {
                if($table != "menu") {
                    $key = $keyText;
                }
                $str = '<div id="prezzo' . $index . '"><span>' . $key . ':</span> ' . $value . '€<br/></div>';
            }
            if($key == "id") {
                $str = '<div class="collapse">' . $str . '</div>';
            }
            echo $str;
        }
        $i++;
    }
}

// controlla se ci sono dei piatti vuoti nei menù.
function checkMenu($value) {
    $bool = ($value == "No primo" || $value == "No secondo" ||
        $value == "No contorno" || $value == "No stuzzichino" ||
        $value == "No dessert" || $value == "No bibita");
    return $bool;
}

// Controlla che tipo di base è per evitare la scritta "base" / "base2" utilizzando stringhe più comprensibili.
function checkType($tipo, $key) {
    if($tipo == "primo") {
        if($key == "base") {
            $key = "tipo di pasta";
        }
        if($key == "base2") {
            $key = "tipo di sugo";
        }
        if($key == "aggiunta_formaggio") {
            $key = "aggiunta formaggio";
        }
    } else if($tipo == "pizza") {
        if($key == "base") {
            $key = "farina";
        }
        if($key == "base2") {
            $key = "base per pizza";
        }
    } else if($tipo == "panino") {
        if($key == "base") {
            $key = "pane";
        }
        if($key == "base2") {
            $key = "salsa";
        }
    } else if($tipo == "ingrediente") {
        if($key == "per_pizza") {
            $key = "per pizza";
        }
        if($key == "per_panino") {
            $key = "per panino";
        }
    }
    if($tipo != "primo" && $tipo != "pizza" && $tipo != "panino") {
        if($key == "add_price") {
            $key = "sovrapprezzo";
        }
    }
    return $key;
}

// funzione usata per aggiungere l'header per un piatto o un elemento.
function setHeader($elem, $index, $table) {
    $text = "";
    if ($table == "menu" || $table == "news") {
        $text = $elem;
    } else {
        switch ($elem) {
            //Tipi di piatto
            case 'stuzzichino':
                $text = 'Stuzzichini';
                break;
            case 'primo':
                $text = 'Primi';
                break;
            case 'pizza':
                $text = 'Pizze';
                break;
            case 'panino':
                $text = 'Panini';
                break;
            case 'contorno':
                $text = 'Contorni';
                break;
            case 'dessert':
                $text = 'Dessert';
                break;
            case 'bibita':
                $text = 'Bibite';
                break;
            //Tipi di elemento
            case 'pasta':
                $text = 'Tipi di pasta';
                break;
            case 'sugo':
                $text = 'Sughi';
                break;
            case 'farina':
                $text = 'Tipi di farina';
                break;
            case 'base_pizza':
                $text = 'Basi per pizza';
                break;
            case 'pane':
                $text = 'Tipi di pane';
                break;
            case 'salsa':
                $text = 'Salse';
                break;
            case 'ingrediente':
                $text = 'Ingredienti';
                break;
        }
    }
    $btn = setHtml($index, $elem);
    $html = '<div class="elemList"><h3>' . $text . '</h3>' . $btn;
    if($table == "menu" || $table == "news") {
      $btn = setHtml($index, $index);
      $html = '<div class="elemList per' . $index . '"><h3>' . $text . '</h3>' . $btn;
    }
    return $html;
}

//Funzione per aggiungere l'html necessario per la creazione del div collassato.
function setHtml($index, $tipo) {
    $btn = '<div class="show">
            <button id="btnShow' . $index . '" class="btn btn-outline-primary btn-circle" onclick="'. "show('" . $tipo . "'" . ')">
            <span class="fa fa-angle-down"></span></button>
          </div>
        </div>
        <div class="collapse" id="coll' . $tipo . '">';
    return $btn;
}

function setModifica($id, $func, $table, $elem) {
    $check = '<div class="custom-control custom-checkbox">
              <input type="checkbox" id="checksconto' . $id . '" name="sconto" class="custom-control-input"/>
              <label class="custom-control-label" for="checksconto' . $id . '">In sconto</label>
            </div>';
    $str = '<div class="collapse container" id="daModificare' . $id . '">
            <div class="center-block">
              <form>
                <label for="modificaPrezzo' . $id . '">Modifica prezzo</label>
                <input type="number" class="form-control input-sm" id="modificaPrezzo' . $id . '" name="prezzo" min="0" step=".01"/>'
        . $check . '<input type="button" id="btn' . $id . '" class="btn btn-outline-primary" value="Modifica" ' . $func . '>
              </form>
            </div>
          </div>';
    if($table == "news") {
        $str = '<div class="collapse container" id="daModificare' . $id . '">
              <div class="center-block">
                <form>
                  <label for="modificaTesto' . $id . '">Modifica Testo</label>
                  <input type="text" class="form-control input-sm" id="modificaTesto' . $id . '" name="testo"/>
                  <input type="button" id="btn' . $id . '" class="btn btn-outline-primary" value="Modifica" ' . $func . '>
                </form>
              </div>
            </div>';
    }
    return $str;
}

// Funzione usata per scegliere cosa visualizzare se per quella categoria non ci sono prodotti.
function scegliParola($tipo, $table) {
    $str = "";
    $parola = "";
    $elem = $tipo;
    if ($table == "menu") {
        $str = "Nessun menù";
    } else if ($table == "news") {
        $str = "Nessuna news";
    } else {
        switch($tipo) {
            case 'stuzzichino':
                $parola = 'Nessuno ';
                break;
            case 'pizza':
            case 'bibita':
            case 'salsa':
                $parola = 'Nessuna ';
                break;
            case 'base_pizza':
                $parola = 'Nessuna ';
                $elem = 'base per pizza';
                break;
            case 'farina':
            case 'pasta':
            case 'pane':
                $parola = 'Nessun ';
                $elem = 'tipo di ' . $tipo;
                break;
            default:
                $parola = 'Nessun ';
                $elem = $tipo;
                break;
        }
        $str = $parola . $elem;
    }
    return $str . '</div>';
}
?>
