<?php
session_start();
$database = include('../php/db/dbconfig.php');
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Aggiungi menù</title>
  <?php include('../include/head.php'); ?>
  <link rel="stylesheet" href="../css/register.css"/>
  <link rel="stylesheet" href="../css/mainStyle.css"/>
  <link rel="stylesheet" href="../css/admin.css"/>
  <script type="text/javascript" src="../js/adminMenu.js"></script>
  <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
  <?php include('../include/adminNavbar.php'); ?>
  <section>
    <h1> Aggiungi menù </h1>
    <p>
      Tramite la seguente form è possibile inserire nuovi menù componendoli con
      i piatti già presenti nel sistema.
    </p>
  </section>
  <div class="container">
    <div class="center-block">
      <form>
        <label for="title">Nome menù: </label>
        <input type="text" id="title" name="MenuName" class="form-control input-sm" required="required"/><br/>
        <?php
          $str="Aggiungi piatti al menù";
          /*Creo l'array di elementi che compongono il menù (primo, secondo, ecc)*/
          $elem=array( "stuzzichino", "primo", "secondo", "contorno", "dessert", "bibita");
          /*Poichè stiamo creando un menù creato dall'admin il prezzo totale
          non sarà la somma dei prezzi dei piatti che lo compongono ma ad essa si
          applicherà uno sconto.*/
          $sconto=true;
          /*Richiamo il file che riempie la select con i piatti che l'admin ha già
          aggiunto nel database. La prima opzione è vuota perchè non tutti i menù
          devono per forza contenere tutte le portate.*/
          include('elemSelection.php');
        ?>
        <div id="prTotale">
          Prezzo totale prodotti:
        </div><br/>
        <div id="sconto">
          Sconto:
        </div>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" id="mod" name="modifica" class="custom-control-input"/>
          <label class="custom-control-label" for="mod">Modifica sconto per questo menù</label>
        </div><br/>
        <div id="nuovo" class="collapse">
        <label for="new">Nuovo sconto: </label>
        <input type="number" id="new" name="sconto" class="form-control input-sm" min="0" step=".01"/><br/>
        </div>
        <div id="prScontato">
          Prezzo menù:
        </div>
        <div class="divbtn">
          <input type="button" class="btn btn-primary" onclick="insertMenu()" data-toggle="modal" data-target="#modal" value="Aggiungi menù"/>
        </div>
        <?php include("../php/modal.php"); ?>
      </form>
    </div>
  </div>
</body>
