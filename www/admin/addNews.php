<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">

<head>
    <title> Add news </title>
    <?php include('../include/head.php'); ?>

    <link rel="stylesheet" href="../css/register.css"/>
    <link rel="stylesheet" href="../css/mainStyle.css"/>
    <link rel="stylesheet" href="../css/admin.css"/>
    <script type="text/javascript" src="../js/addNews.js"></script>
    <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
<?php include('../include/adminNavbar.php'); ?>
<section>
    <h1>Aggiungi news</h1>
    <p>
        Tramite la seguente form è possibile aggiungere le news che si vogliono mostrare ai clienti.
    </p>
</section>
<div class="container">
    <div class="center-block">
        <div class="form-group">
            <label for="titolo">Titolo:</label>
            <input type="text" class="form-control" id="titolo" placeholder="Titolo">
        </div>
        <div class="form-group">
            <label for="testo">Testo:</label><br/>
            <textarea id="testo" placeholder="Testo notizia"></textarea>
        </div>
        <div class="divbtn">
            <button onclick="postnews()" class="btn btn-primary" data-toggle="modal" data-target="#modal">Aggiungi notizia</button>
        </div>
    </div>
</div>
<?php include("../php/modal.php"); ?>
</body>

</html>
