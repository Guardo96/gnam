<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Aggiungi piatto</title>
    <?php include('../include/head.php'); ?>
    <link rel="stylesheet" href="../css/register.css"/>
    <link rel="stylesheet" href="../css/foodList.css"/>
    <link rel="stylesheet" href="../css/mainStyle.css"/>
    <link rel="stylesheet" href="../css/admin.css"/>
    <script src="../js/adminDish.js" type="text/javascript"></script>
    <script src="../js/createDish.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
<?php include('../include/adminNavbar.php'); ?>
<div id="user" class="collapse" >admin</div>
<section>
    <h1>Aggiungi piatti</h1>
    <p>
        Tramite la seguente form è possibile aggiungere nuovi piatti ordinabili.
    </p>
</section>
<div class="container">
    <div class="center-block">
        <?php
        $fileIngr = "../php/IngrQuery.php";
        $filePrice = "../php/priceQuery.php";
        ?>
        <form action="#">
            <fieldset>
                <div> Tipo piatto: </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="primo" value="primo" name="type" class="custom-control-input" onclick="selectDish('#primo', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="primo">Primo</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="secondo" value="secondo" name="type" class="custom-control-input" onclick="selectDish('#secondo', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="secondo">Secondo</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="contorno" value="contorno" name="type" class="custom-control-input" onclick="selectDish('#contorno', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="contorno">Contorno</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="stuzzichino" value="stuzzichino" name="type" class="custom-control-input" onclick="selectDish('#stuzzichino', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="stuzzichino">Stuzzichino</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="dessert" value="dessert" name="type" class="custom-control-input" onclick="selectDish('#dessert', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="dessert">Dessert</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="bibita" value="bibita" name="type" class="custom-control-input" onclick="selectDish('#bibita', '<?php echo $fileIngr; ?>')" required="required"/>
                    <label class="custom-control-label" for="bibita">Bibita</label>
                </div>
            </fieldset>
            <fieldset id="elemDish">
                <div id="divImg"></div>
                <div class="collapse classAddInfo" id="infoName">
                    <label for="nomePiatto">nome piatto:</label>
                    <input type="text" id="nomePiatto" name="nome" class="form-control input-sm"/><br/>
                </div>
                <div class="collapse" id="divPrimo">
                    <?php include('../php/primo.php'); ?>
                </div>
                <div class="collapse" id="divSecondo">
                    <?php include('../php/secondo.php'); ?>
                </div>
                <div class="collapse modificaPrezzo">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="mod" name="modifica" class="custom-control-input" onclick="modifyPrice()"/>
                        <label class="custom-control-label" for="mod">Modifica prezzo per questo piatto</label>
                    </div><br/>
                    <div id="nuovo" class="collapse">
                        <label for="new">Nuovo prezzo: </label>
                        <input type="number" id="new" name="newPrice" class="form-control input-sm" min="0" step=".01"/><br/>
                    </div>
                </div>
                <div class="collapse classAddInfo" id="infoPrice">
                    <label for="price"> Inserisci prezzo: </label>
                    <input type="number" id="price" name="euro" class="form-control input-sm" min="0" step=".01"/>
                </div>
            </fieldset><br/>
            <div id="divAdd" class="collapse divbtn">
                <input type="button" id="add" class="btn btn-primary" onclick="insertDish()" data-toggle="modal" data-target="#modal" value="Aggiungi piatto"/>
            </div>
            <?php include("../php/modal.php"); ?>
        </form>
        <div id="prezzoTotale" class="collapse">0</div>
        <div id="prezzoIngrediente0" class="collapse">0</div>
        <div id="prezzoIngrediente1" class="collapse">0</div>
        <div id="prezzoIngrediente2" class="collapse">0</div>
        <div id="prezzoIngrediente3" class="collapse">0</div>
        <div id="prezzoIngrediente4" class="collapse">0</div>
        <div id="prezzoIngrediente5" class="collapse">0</div>
        <div id="prezzoBase1" class="collapse">0</div>
        <div id="prezzoBase2" class="collapse">0</div>
    </div>
</div>

</body>
