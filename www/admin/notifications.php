<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Notifiche Admin</title>
    <?php include('../include/head.php'); ?>
    <link rel="stylesheet" href="../css/mainStyle.css"/>
    <link rel="stylesheet" href="../css/account.css"/>
    <script type="text/javascript" src="../js/notification.js"></script>
</head>
<body>
<?php include('../include/adminNavbar.php'); ?>
<div id="utente" class="collapse">admin</div>
<h1>Notifiche Admin</h1>
<div class="divgrad">
    <h2>Ordini da eseguire</h2>
    <div id="daEseguire"></div>
    <div id="daEsec"></div>
</div>
<div class="divgrad">
    <h2>Ordini in esecuzione</h2>
    <div id="inEsecuzione"></div>
    <div id="inEsec"></div>
</div>
</body>
</html>

<?php
function cambiastato($id, $stato)
{
    try {
        $database = include('../php/db/dbconfig.php');
        $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $conn->prepare("UPDATE ordine SET stato = '$stato' WHERE id = '$id';");
        $statement->execute();
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}
?>
