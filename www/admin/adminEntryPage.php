<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">

<head>
    <title> Administrator </title>
    <?php include('../include/head.php'); ?>
    <link rel="stylesheet" href="../css/mainStyle.css"/>
    <link rel="stylesheet" href="../css/entryPage.css"/>
    <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
  <?php include('../include/adminNavbar.php'); ?>
  <div class="container">
    <form>
      <div class="btn-group-vertical">
          <input type="button" id="ingr" class="btn btn-primary btn-lg" value="Aggiungi elementi"
                 onclick="window.location.href = 'addIngr.php'"/>
          <input type="button" id="dish" class="btn btn-primary btn-lg" value="Aggiungi piatti"
                 onclick="window.location.href = 'addDish.php'"/>
          <input type="button" id="menu" class="btn btn-primary btn-lg" value="Aggiungi menù"
                 onclick="window.location.href = 'addMenu.php'"/>
          <input type="button" id="news" class="btn btn-primary btn-lg" value="Aggiungi news"
                 onclick="window.location.href = 'addNews.php'"/>
      </div>
    </form>
  </div>
</body>

</html>
