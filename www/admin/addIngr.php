﻿<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Aggiungi ingrediente</title>
    <?php include('../include/head.php'); ?>
    <link rel="stylesheet" href="../css/register.css"/>
    <link rel="stylesheet" href="../css/mainStyle.css"/>
    <link rel="stylesheet" href="../css/admin.css"/>
    <script type="text/javascript" src="../js/addIngr.js"></script>
    <script type="text/javascript" src="../js/navbar.js"></script>
</head>
<body>
<?php include('../include/adminNavbar.php'); ?>
<section>
    <h1> Aggiungi ingredienti </h1>
    <p>
        Tramite la seguente form è possibile inserire diversi tipi di ingredienti, tra cui i
        diversi tipi di pasta ordinabili, i sughi con cui condirle, le basi per pizze e panini
        e ingredienti extra che possono essere aggiunti ai piatti.
    </p>
</section>
<div class="container">
    <div class="center-block">
        <form action="#" id="ingrForm" >
            <label for="name">Nome Ingrediente: </label>
            <input type="text" id="nome" name="ingrName" class="form-control input-sm" required="required"/><br/>
            <fieldset>
                <div>Tipo Ingrediente: </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="base_panino" name="type" class="custom-control-input" value="base_panino" onclick="changeType('pane')" required="required"/>
                    <label class="custom-control-label" for="base_panino">Pane</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="base_pizza" name="type" class="custom-control-input" value="base_pizza" onclick="changeType('base_pizza')" required="required"/>
                    <label class="custom-control-label" for="base_pizza">Base per pizza</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="ingr" name="type" class="custom-control-input" value="ingrediente" onclick="changeType('ingrediente')" required="required"/>
                    <label class="custom-control-label" for="ingr">Ingrediente</label>
                </div>
                <div class="collapse" id="divScelta">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="per_pizza" id="per_pizza" value="pizza" class="custom-control-input"/>
                        <label for="per_pizza" class="custom-control-label">Per pizza</label><br/>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="per_panino" id="per_panino" value="pizza" class="custom-control-input"/>
                        <label for="per_panino" class="custom-control-label">Per panino</label><br/>
                    </div>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="pasta" name="type" class="custom-control-input" value="pasta" onclick="changeType('pasta')" required="required"/>
                    <label class="custom-control-label" for="pasta">Tipo di pasta</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="sugo" name="type" class="custom-control-input" value="sugo" onclick="changeType('sugo')" required="required"/>
                    <label class="custom-control-label" for="sugo">Tipo di sugo</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="farina" name="type" class="custom-control-input" value="farina" onclick="changeType('farina')" required="required"/>
                    <label class="custom-control-label" for="farina">Farina</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="salsa" name="type" class="custom-control-input" value="salsa" onclick="changeType('salsa')" required="required"/>
                    <label class="custom-control-label" for="salsa">Salsa</label>
                </div>
            </fieldset><br/>
            <fieldset>
                <label for="euroPrice">Prezzo: </label>
                <input type="number" id="euroPrice" name="euro" class="form-control input-sm" min="0" step=".01" required="required"/>
            </fieldset>
            <div class="divbtn">
                <input type="button" id="add" class="btn btn-primary" data-toggle="modal" data-target="#modal" value="Aggiungi"/>
            </div>
            <?php include("../php/modal.php"); ?>
        </form>
    </div>
</div>
</body>
</html>
