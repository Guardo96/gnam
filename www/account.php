<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <?php include('include/head.php'); ?>
    <?php $utente = $_SESSION['user']; ?>
    <title>Account (<?php echo $utente; ?>)</title>
    <link href="css/mainStyle.css" rel="stylesheet"/>
    <link href="css/account.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/navbar.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<h1>Dettagli account di <?php echo $utente; ?></h1>
<div class="divgrad">
    <h2>Info generali</h2>
    <?php infoutente() ?>
</div>
<div>
    <h2>Ordini</h2>
    <?php infoordini() ?>
</div>
</body>
</html>

<?php
//QUERY

function infoutente()
{
    $utente = $_SESSION['user'];

    try {
        $database = include('php/db/dbconfig.php');
        $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $conn->prepare("SELECT * FROM utente WHERE username = '$utente'");
        $statement->execute();
        while ($row = $statement->fetch()) {
            echo "Nome : ", $row['nome'], "<br>";
            echo "Cognome : ", $row['cognome'], "<br>";
            echo "Indirizzo : ", $row['indirizzo'], "<br>";
            echo "Carta : ", $row['carta'], "<br>";
            echo "Email : ", $row['email'], "<br>";
        }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

}

function infoordini()
{
    $utente = $_SESSION['user'];

    try {
        $database = include('php/db/dbconfig.php');
        $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $conn->prepare("SELECT * FROM ordine WHERE username = '$utente'");
        $statement->execute();
        while ($row = $statement->fetch()) {
            $i = $row['id'];
            $statement2 = $conn->prepare("SELECT * FROM prodottoInOrdine WHERE id_ordine = '$i'");
            $statement2->execute();
            $count2 = $statement2->rowCount();
            if ($count2 == 0) {
              echo "Errore ";
            } else { ?>
            <div class="divgrad">
            <section class="food<?php echo $i; ?>">
            <p id="ordine<?php echo $i; ?>" class="pOrdine"><span>Ordine N <?php echo $row['id']; ?></span></p>
            <p id="status<?php echo $i; ?>" class="pOrdine"><span>Stato: <?php echo $row['stato']; ?></span></p>
            <div id="info<?php echo $i; ?>" class="divInfo">
            <ul>
            <?php while ($row2 = $statement2->fetch()) { ?>
            <p id="prodotto<?php echo $i; ?>" class="pprodotto"><li> <?php echo $row2['prodotto']; ?>
            <?php if($row2['quantita']>1){?> x<?php echo $row2['quantita']; } ?></li></p> <?php } ?>
            </ul>
            <p id="price<?php echo $i; ?>" class="pPrice"><?php echo $row['prezzo']; ?> €</p>
            <?php if($row['stato'] == "pronto"){ ?>
            <input type="button" id="esec<?php echo $i; ?>" class="btn btn-primary" onclick="archivia(<?php echo $i; ?>)" value="Archivia"/>
            <?php } ?>
            <p>-------------------------------------------------------</p>
            </div>
            </section>
            </div>
            <?php
            }
      }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

}


?>
