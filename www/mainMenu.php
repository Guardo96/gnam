<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Main menu</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/entryPage.css"/>
    <link rel="stylesheet" href="css/account.css"/>
    <script type="text/javascript" src="js/navbar.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<div class="container">
    <form>
        <div class="col btn-group-vertical">
            <input type="button" id="listaCibo" class="btn btn-primary btn-lg" value="HO FAME!"
                   onclick="window.location.href='foodMenu.php'"/>
            <input type="button" id="listaMenu" class="btn btn-primary btn-lg" value="I Nostri Menù"
                   onclick="window.location.href='menuList.php'"/>
        </div>
    </form>
</div>
<div class="divgrad" id="news">
    <h2>NOTIZIE</h2>
    <?php getnotizie(); ?>
</div>
</body>
</html>

<?php

function getnotizie()
{
    $database = include('php/db/dbconfig.php');
    try {
        //connect to the database
        $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = $conn->prepare("SELECT * FROM news");
        $query->execute();
        if ($query->rowCount() != 0) {
            while ($row = $query->fetch()) {
                echo "<h3>";
                echo $row['titolo'];
                echo "</h3><br/><div>";
                echo $row['testo'], "<br/>";
                echo "(", $row['tempo'], ")<br/></div>";
            }
        } else {
            echo "NOTIZIE NON DISPONIBILI";
        }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

?>
