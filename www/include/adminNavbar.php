<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <a class="navbar-brand" href="#"><img id="navimg" src="../res/logo/sandwich_line.png" height="64" width="64"/>
        (admin)</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="adminEntryPage.php"><span class="fas fa-home"></span> Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="everything.php"><span class="fas fa-eye"></span> Visualizza tutto</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="notifications.php"><span class="fas fa-exclamation-circle"></span> Notifiche (<span id="numberNotify">0</span>)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../logout.php"><span class="fas fa-sign-out-alt"></span> Logout</a>
            </li>
        </ul>
    </div>
    <div id="SessionUser"  class="collapse">admin</div>
</nav>
