<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <a class="navbar-brand" href="#"><img id="navimg" src="res/logo/sandwich_line.png" alt="logo gnam tutti a tavola" height="64" width="64"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="mainMenu.php"><span class="fas fa-home"></span> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="account.php"><span class="fas fa-user"></span> Account (<?php echo $_SESSION['user']; ?>)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="notifications.php"><span class="fas fa-exclamation-circle"></span> Notifiche (<span id="numberNotify">0</span>)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="cart.php"><span class="fas fa-utensils"></span> Carrello</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php"><span class="fas fa-sign-out-alt"></span> Logout</a>
            </li>
        </ul>
    </div>
    <div id="SessionUser"  class="collapse"><?php echo $_SESSION['user']; ?></div>
</nav>
