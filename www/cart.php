<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Carrello</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/cart.css"/>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<?php $utente = $_SESSION['user']; ?>
<div id="username" class="collapse"><?php echo $utente; ?></div>
<h1>Riepilogo ordine</h1>
<div class="divbtn btn-group" id="group">
    <button class="btn-primary" onclick="window.location.href= 'menuList.php'">Torna ai menù</button>
    <button class="btn-primary" onclick="window.location.href= 'foodMenu.php'">Torna ai piatti</button>
    <button class="btn-primary" onclick="window.location.href= 'foodList.php?request=load&categoria=bibita'">Torna alle bibite</button>
</div>
<form id="formCart">
    <h2>Menù</h2>
    <div class="menu" id="menu">
    </div>
    <h2>Piatti</h2>
    <div class="prod" id="prod">
    </div>
    <h2>Bibite</h2>
    <div class="bibite" id="bibite">
    </div>
    <div id="prTotale">
        Prezzo totale prodotti: <div></div>
    </div>
    <div class="divbtn" id="conferma">
      <input type="button" class="btn btn-primary conferma" value="Conferma ordine" data-toggle="modal" data-target="#modal" onclick="inviaOrdine()"/>
    </div>
    <?php include("php/modal.php"); ?>
</form>
</body>
</html>
