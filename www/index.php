<?php
session_start();
if (isset($_COOKIE["rememberme"])) {
    header("location: mainMenu.php");
}
?>
<!doctype html>
<html lang="it">
<head>
    <title>GNAM</title>
    <?php include('include/head.php'); ?>
    <link href="css/mainStyle.css" rel="stylesheet"/>
    <link href="css/index.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
    <script type="text/javascript" src="js/resetSessionStorage.js"></script>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>

</head>

<body>

<div role="main">

    <div class="jumbotron text-center">
        <div class="container">
            <img src="res/logo/gnam_full.png" alt="logo gnam tutti a tavola" height="250" width="300"/>
            <p>
                <a href="login.php" class="btn btn-primary btn-lg">Accedi</a>
                <a href="register.php" class="btn btn-primary btn-lg">Registrati</a>
            </p>
        </div>
    </div>

    <div class="album py-5 bg-light">
        <div class="container">
            <p>Qui puoi ordinare...</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://images.lacucinaitaliana.it/wp-content/uploads/2015/04/carbonara.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://images.lacucinaitaliana.it/wp-content/uploads/2015/04/carbonara.jpg"
                                 alt="pasta"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Pasta fatta in casa</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Pasta</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://cdnb.ricettedigusto.info/2016/02/Patate-fritte-croccanti.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://cdnb.ricettedigusto.info/2016/02/Patate-fritte-croccanti.jpg"
                                 alt="stuzzichini"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Stuzzichini sfiziosi</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Stuzzichini</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg/800px-Eq_it-na_pizza-margherita_sep2005_sml.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg/800px-Eq_it-na_pizza-margherita_sep2005_sml.jpg"
                                 alt="pizza"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">La vera pizza italiana</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Pizza</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://upload.wikimedia.org/wikipedia/commons/e/e3/Salmon_Cream_Cheese_Sandwiches.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://upload.wikimedia.org/wikipedia/commons/e/e3/Salmon_Cream_Cheese_Sandwiches.jpg"
                                 alt="panino"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Panini croccanti e saporiti</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Il nostro panino</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://www.laprimacatering.com/images/Product/large/8050.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://www.laprimacatering.com/images/Product/large/8050.jpg"
                                 alt="bibite"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Bibite rinfrescati</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Bibita</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <a href="https://upload.wikimedia.org/wikipedia/commons/c/c3/Berliner_Luft.jpg"
                           data-toggle="lightbox" data-gallery="example-gallery" data-max-width="600">
                            <img class="card-img-top"
                                 src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Berliner_Luft.jpg"
                                 alt="dessert"
                                 height="250" width="300">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Dessert ricercati</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">Dessert</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="text-muted">
    <div class="container">
        <div class="row">
            <div class="col">
                <p>CONTATTACI</p>
                <ul>
                    <li>Tel : 0547-123456</li>
                    <li>Fax : 0547-789101</li>
                    <li><span class="fab fa-facebook"></span> <span class="fab fa-twitter"></span> <span class="fab fa-youtube"></span> <span
                                class="fab fa-instagram"></span> <span class="fab fa-pinterest-square"></span> <span
                                class="fab fa-tumblr-square"></span></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p>DOVE SIAMO</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d551.9650918543801!2d12.243261586331927!3d44.140283659091416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132ca4c8a9e0b5cb%3A0xbbd5902e81162eed!2sUniversit%C3%A0+di+Bologna+-+Dipartimento+di+Informatica+-+Ingegneria+E+Scienze+Informatiche!5e0!3m2!1sit!2sit!4v1528443886435"
                        height="400" allowfullscreen></iframe>
                <p><a href="#">Back to top</a></p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
