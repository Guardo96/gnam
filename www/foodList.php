<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Lista piatti</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/foodList.css"/>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/list.js"></script>
    <script type="text/javascript" src="js/createDish.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<div id="user" class="collapse" ><?php echo $_SESSION['user']; ?> </div>
<h1><?php echo $_GET['categoria'];?></h1>
<?php
if ($_GET['categoria'] == 'primo' || $_GET['categoria'] == 'secondo'): ?>
    <h2>Crea il tuo piatto</h2>
<?php endif; ?>
<?php
if ($_GET['categoria'] == 'primo' || $_GET['categoria'] == 'secondo') {
    include('php/createClientDish.php');
}
?>
<h2>Scegli tra le proposte</h2>
<?php if($_GET['categoria'] == 'secondo'): ?>
    <div class="container">
        <div class="center-block">
            <form>
                <fieldset>
                    <legend class="collapse">Scelta secondo</legend>
                    <div id="divCheck">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="tutti" value="tutti" name="scelta" class="custom-control-input" onclick="selectSecondi('tutti')"/>
                            <label class="custom-control-label" for="tutti">Tutti i secondi</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="pizze" value="pizze" name="scelta" class="custom-control-input" onclick="selectSecondi('pizza')"/>
                            <label class="custom-control-label" for="pizze">Solo le pizze</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="panini" value="panini" name="scelta" class="custom-control-input" onclick="selectSecondi('panino')"/>
                            <label class="custom-control-label" for="panini">Solo i panini</label>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
<?php endif; ?>
<?php include('php/createFoodList.php'); ?>
<?php
$prec = 'mainMenu.php';
$pag = $prec;
$titoloPrec = 'Home';
$titoloPag = 'Home';
switch ($_GET['categoria']) {
    case 'primo':
        $pag = 'foodList.php?request=load&categoria=secondo';
        $titoloPag = 'Secondi';
        break;
    case 'secondo':
        $prec = 'foodList.php?request=load&categoria=primo';
        $titoloPrec = 'Primi';
        $pag = 'foodList.php?request=load&categoria=contorno';
        $titoloPag = 'Contorni';
        break;
    case 'contorno':
        $prec = 'foodList.php?request=load&categoria=secondo';
        $titoloPrec = 'Secondi';
        $pag = 'foodList.php?request=load&categoria=stuzzichino';
        $titoloPag = 'Stuzzichini';
        break;
    case 'stuzzichino':
        $prec = 'foodList.php?request=load&categoria=contorno';
        $titoloPrec = 'Contorni';
        $pag = 'foodList.php?request=load&categoria=dessert';
        $titoloPag = 'Dessert';
        break;
    case 'dessert':
        $prec = 'foodList.php?request=load&categoria=stuzzichino';
        $titoloPrec = 'Stuzzichini';
        $pag = 'foodList.php?request=load&categoria=bibita';
        $titoloPag = 'Bibite';
        break;
    case 'bibita':
        $prec = 'foodList.php?request=load&categoria=dessert';
        $titoloPrec = 'Bibite';
        break;
}
?>
<div class="divbtn" id="scegliPag">
    <div class="sinistra">
        <button class="btn btn-outline-primary btn-circle btn-xl" id="indietro" onclick="window.location.href = '<?php echo $prec; ?>'"><span class="fa fa-angle-left"></span></button>
        <div id="divInd"> <span><?php echo $titoloPrec; ?></span></div>
    </div>
    <div class="destra">
        <div id="divAv"> <span><?php echo $titoloPag; ?></span></div>
        <button class="btn btn-outline-primary btn-circle btn-xl" id="avanti" onclick="window.location.href = '<?php echo $pag; ?>'"><span class="fa fa-angle-right"></span></button>
    </div>
</div>
<div class="divbtn cambiaPag">
    <div class="sinistra">
        <button class="btn btn-outline-primary btn-circle btn-xl" id="aiMenu" onclick="window.location.href = 'menuList.php'"><span class="fa fa-angle-left"></span></button>
        <div id="divaimenu"> <span>Menù</span></div>
    </div>
    <div class="destra">
        <div id="divalcarr"> <span>Carrello</span></div>
        <button class="btn btn-outline-primary btn-circle btn-xl" id="alCarrello" onclick="window.location.href = 'cart.php'"><span class="fa fa-angle-right"></span></button>
    </div>
</div>
<?php if($_GET['categoria'] == "primo" || $_GET['categoria'] == "secondo"){ ?>
    <div id="prezzoTotale" class="collapse">0</div>
    <div id="prezzoIngrediente0" class="collapse">0</div>
    <div id="prezzoIngrediente1" class="collapse">0</div>
    <div id="prezzoIngrediente2" class="collapse">0</div>
    <div id="prezzoIngrediente3" class="collapse">0</div>
    <div id="prezzoIngrediente4" class="collapse">0</div>
    <div id="prezzoIngrediente5" class="collapse">0</div>
    <div id="prezzoBase1" class="collapse">0</div>
    <div id="prezzoBase2" class="collapse">0</div>
<?php } ?>
</body>
</html>
