<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Accedi</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/login.css"/>
</head>

<body>
<header><img src="res/logo/gnam_full.png" alt="logo gnam tutti a tavola" height="250" width="300"/></header>
<div class="container">
    <form action="php/checkLogin.php" method="post">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Inserisci Email" name="email">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" placeholder="Inserisci Password" name="password">
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name="ricordami" id="ricorda" class="custom-control-input"/>
            <label class="custom-control-label" for="ricorda">Ricordami</label>
        </div>
        <div class="divbtn">
            <input type="submit" class="btn btn-primary" id="login" value="Entra"/>
        </div>
    </form>
</div>
</body>
</html>
