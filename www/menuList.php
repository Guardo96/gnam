<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Lista menù</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/foodList.css"/>
    <link rel="stylesheet" href="css/menuList.css"/>
    <script type="text/javascript" src="js/list.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<h1>I nostri menù</h1>
<?php include('php/createMenuList.php'); ?>
<div class="divbtn cambiaPag">
    <div class="sinistra">
        <button class="btn btn-outline-primary btn-circle btn-xl" id="aiPiatti" onclick="window.location.href = 'foodMenu.php'"><span class="fa fa-angle-left"></span></button>
        <div id="divaipiatti"> <span>Piatti</span></div>
    </div>
    <div class="destra">
        <div id="divalcarr"> <span>Carrello</span></div>
        <button class="btn btn-outline-primary btn-circle btn-xl" id="alCarrello" onclick="window.location.href = 'cart.php'"><span class="fa fa-angle-right"></span></button>
    </div>
</div>
</body>
</html>
