<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Registrazione</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/register.css"/>
    <script type="text/javascript" src="js/checkReg.js"></script>
</head>
<body>
<header><img src="res/logo/gnam_full.png" alt="logo gnam tutti a tavola" height="250" width="300"/></header>
<div class="container">
    <div class="center-block">
        <form action="#" id="form">
            <label for="nome">Nome:</label>
            <input class="form-control input-sm" type="text" name="firstname" id="nome" required="required"/><br/>
            <label for="cognome">Cognome:</label>
            <input class="form-control input-sm" type="text" name="lastname" id="cognome" required="required"/><br/>
            <label for="indirizzo">Indirizzo:</label>
            <input class="form-control input-sm" type="text" name="address" id="indirizzo" required="required"/><br/>
            <label for="username">Username:</label>
            <input class="form-control input-sm" type="text" name="username" id="username" required="required"/><br/>
            <label for="carta">Codice carta per pagamento:</label>
            <input class="form-control input-sm" type="text" name="carta" id="carta" required="required"/><br/>
            <label for="email">Email:</label>
            <div class="alert alert-danger" id="alert">Errore! E-mail non corretta.</div>
            <input class="form-control input-sm" type="email" name="email" id="email" required="required"/><br/>
            <label for="password">Password:</label>
            <div class="alert alert-warning" id="lunghezza">Attenzione! La password deve contenere almeno 8 caratteri.</div>
            <div class="alert alert-warning" id="caratteri">
                Attenzione! La password deve contenere almeno una lettera maiuscola o un numero.
            </div>
            <input class="form-control input-sm" type="password" name="password" id="password" required="required"/><br/>
            <label for="confermapsw">Conferma password:</label>
            <div class="alert alert-danger" id="alert2">Errore! Password non valida.</div>
            <input class="form-control input-sm" type="password" name="confermapsw" id="confermapsw" required="required"/><br/>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="privacy" id="privacy" required="required"/>
                <label for="privacy" class="custom-control-label">Accetta condizioni sulla privacy</label><br/>
            </div>
            <div class="divbtn">
                <input type="button" class="btn btn-primary" id="reg" data-toggle="modal" data-target="#mod" value="Registrati"/>
            </div>
            <div class="modal fade" id="mod" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h2 class="modal-title">...</h2>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <p id="modcontent"></p>
                  </div>
                  <div class="modal-footer divbtn">
                    <div class="collapse" id="success">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="window.location.href='index.php'">Torna alla Home</button>
                    </div>
                    <div class="collapse" id="wrong">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Torna al form</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
