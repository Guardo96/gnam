<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Main menu</title>
    <?php include('include/head.php'); ?>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/entryPage.css"/>
    <script type="text/javascript" src="js/navbar.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<div class="container">
    <form>
        <div class="btn-group-vertical">
            <input type="button" id="primi" class="btn btn-primary btn-lg" value="PRIMI" onclick="window.location.href = 'foodList.php?request=load&categoria=primo'"/>
            <input type="button" id="secondi" class="btn btn-primary btn-lg" value="SECONDI" onclick="window.location.href = 'foodList.php?request=load&categoria=secondo'"/>
            <input type="button" id="contorni" class="btn btn-primary btn-lg" value="CONTORNI" onclick="window.location.href = 'foodList.php?request=load&categoria=contorno'"/>
            <input type="button" id="stuzzichino" class="btn btn-primary btn-lg" value="STUZZICHINI" onclick="window.location.href = 'foodList.php?request=load&categoria=stuzzichino'"/>
            <input type="button" id="dessert" class="btn btn-primary btn-lg" value="DESSERT" onclick="window.location.href = 'foodList.php?request=load&categoria=dessert'"/>
            <input type="button" id="bibite" class="btn btn-primary btn-lg" value="BIBITE" onclick="window.location.href = 'foodList.php?request=load&categoria=bibita'"/>
        </div>
    </form>
</div>
</body>
</html>
