<?php session_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
    <?php
    include('include/head.php');
    $utente = $_SESSION['user'];
    ?>
    <title>Notifiche Account (<?php echo $utente; ?>)</title>
    <link rel="stylesheet" href="css/mainStyle.css"/>
    <link rel="stylesheet" href="css/account.css"/>
    <script type="text/javascript" src="js/userNotification.js"></script>
</head>
<body>
<?php include('include/navbar.php'); ?>
<div id="utente" class="collapse" ><?php echo $utente; ?></div>
<h1>Notifiche di <?php echo $utente; ?></h1>
<div class="divgrad">
    <div id="inviati">
      <h2>Ordini inviati</h2>
      <div id="ordini1"></div>
    </div>
    <div id="inEsecuzione">
     <h2>Ordini in Esecuzione</h2>
     <div id="ordini2"></div>
    </div>
    <div id="pronti">
    <h2>Ordini Pronti</h2>
    <div id="ordini3"></div>
    </div>
</div>
</body>
</html>
