<?php
if($_GET['request'] == "load"){
    $utente = $_SESSION['user'];
?>
<div class="container" id="listaPiatti">
<?php
} else {
    $utente = $_GET['user'];
}
?>
<?php
  $database = include('db/dbconfig.php');

  $cat = $_GET['categoria'];
  try {
      $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   if($cat == 'primo'){
        $statement = $conn->prepare("SELECT * FROM piatto WHERE tipo = '$cat' AND (username = 'admin' OR username = '$utente')");
        $statement->execute();
   } else if($cat == 'secondo'){
        $statement = $conn->prepare("SELECT * FROM piatto WHERE (tipo = 'panino' OR tipo = 'pizza') AND (username = 'admin' OR username = '$utente')");
        $statement->execute();
   } else {
         $statement = $conn->prepare("SELECT * FROM vario WHERE tipo = '$cat'");
         $statement->execute();
    }
  } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
  }
  $i = 0;
  $isMenu = false;
  while ($row = $statement->fetch()) {
?>
    <div class="food">
      <div id="info<?php echo $i; ?>" class="divInfo">
        <div class="vedisconto">
          <?php if($row['sconto'] == "Y"): ?>
          <div class="insconto">
            <span class="fas fa-exclamation"> </span>
          </div>
          <?php endif; ?>
          <p id="name<?php echo $i; ?>" class="pName"><?php echo $row['nome']; ?></p>
        </div>
        <p id="price<?php echo $i; ?>" class="pPrice"><?php echo $row['prezzo']; ?> €</p>
      </div>
      <div id="amount<?php echo $i; ?>" class="divAmount">
        <button id="less<?php echo $i; ?>" class=" buttonLess btn btn-primary btn-circle" onclick="remove(<?php echo $i; ?>)"><span class="fa fa-minus"></span></button>
        <div id="num<?php echo $i; ?>" class="divNum">0</div>
        <button id="more<?php echo $i; ?>" class=" buttonMore btn btn-primary btn-circle" onclick="add(<?php echo $i; ?>)"><span class="fa fa-plus"></span></button>
      </div>
      <?php  if($row['tipo'] == "primo"){ ?>
        <div id="aggiuntaform<?php echo $i; ?>" class="collapse"><?php echo $row['aggiunta_formaggio']; ?></div>
      <?php }?>
    </div>
<?php
$i++;
  }
?>
<div id="numeroProdotti" class="collapse"><?php echo $i; ?></div>
<?php
if($_GET['request'] == "load"){
?>
</div>
<?php } ?>
