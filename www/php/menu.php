<div class="food">
  <div id="info<?php echo $i; ?>" class="divInfo">
    <p id="nome<?php echo $i; ?>" class="pName"><span><?php echo $row['nome']; ?></span></p>
    <p id="price<?php echo $i; ?>" class="pPrice"><?php echo $row['prezzo']; ?> €</p>
  </div>
  <div id="name<?php echo $i; ?>" class="collapse" ><?php echo $row['nome']; ?></div>
  <div id="show<?php echo $i; ?>">
    <button id="btnShow<?php echo $i; ?>" class="btn btn-outline-primary btn-circle" onclick="show(<?php echo $i; ?>)"><span class="fa fa-angle-down"></span></button>
  </div>
</div>
<div id="coll<?php echo $i; ?>" class="collapse">
  <div id="elem<?php echo $i; ?>" class="divElem">
    <?php if($row['stuzzichino'] != "No stuzzichino"){ ?>
    <p id="stuzzichino<?php echo $i; ?>"> <span>stuzzichino:</span> <?php echo $row['stuzzichino']; ?></p>
  <?php } if($row['primo'] != "No primo"){ ?>
    <p id="primo<?php echo $i; ?>"> <span>primo:</span> <?php echo $row['primo']; ?></p>
  <?php } if($row['secondo'] != "No secondo"){ ?>
    <p id="secondo<?php echo $i; ?>"> <span>secondo:</span> <?php echo $row['secondo']; ?></p>
  <?php } if($row['contorno'] != "No contorno"){ ?>
    <p id="contorno<?php echo $i; ?>"> <span>contorno:</span> <?php echo $row['contorno']; ?></p>
  <?php } if($row['dessert'] != "No dessert"){ ?>
    <p id="dessert<?php echo $i; ?>"><span>dessert:</span> <?php echo $row['dessert']; ?></p>
  <?php } if($row['bibita'] != "No bibita"){ ?>
    <p id="bibita<?php echo $i; ?>"><span>bibita:</span> <?php echo $row['bibita']; ?></p>
    <?php } ?>
  </div>
    <div id="amount<?php echo $i; ?>" class="divAmount">
      <button id="less<?php echo $i; ?>" class=" buttonLess btn btn-primary btn-circle" onclick="remove(<?php echo $i; ?>)"><span class="fa fa-minus"></span></button>
      <div id="num<?php echo $i; ?>" class="divNum">0</div>
      <button id="more<?php echo $i; ?>" class=" buttonMore btn btn-primary btn-circle" onclick="add(<?php echo $i; ?>)"><span class="fa fa-plus"></span></button>
    </div>
</div>
