<?php

$database = include('dbconfig.php');


try {
    //open connection
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // TABELLA USER
    $sql = "CREATE TABLE IF NOT EXISTS utente (
    username VARCHAR(255) NOT NULL,
    nome VARCHAR(255) NOT NULL,
    cognome VARCHAR(255) NOT NULL,
    indirizzo VARCHAR(255) NOT NULL,
    carta VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(username)
    )";
    // use exec() because no results are returned
    $conn->exec($sql);
    echo "Tabella user creato con successo </br>";
//--------------------------------------------------------
    // TABELLA MENU
    $sq2 = "CREATE TABLE IF NOT EXISTS menu (
    id INT(10) AUTO_INCREMENT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    stuzzichino VARCHAR(255),
    primo VARCHAR(255),
    secondo VARCHAR(255),
    contorno VARCHAR(255),
    dessert VARCHAR(255),
    bibita VARCHAR(255),
    prezzo FLOAT(7,2) NOT NULL,
    sconto ENUM('Y','N') NOT NULL,
    PRIMARY KEY(id)
    )";
    // use exec() because no results are returned
    $conn->exec($sq2);
    echo "Tabella menu creato con successo </br>";
//-----------------------------------------------------------
    // TABELLA PIATTO
    //BASE: primo->pasta | pizza->tipi di farina | panino-> tipi di pane
   //BASE2: primo->sugo | pizza->margherita,bianca,rossa,fornarina | panino ->salsa

    $sq3 = "CREATE TABLE IF NOT EXISTS piatto (
    id INT AUTO_INCREMENT NOT NULL,
    username VARCHAR(255) NOT NULL,
    nome VARCHAR(255) NOT NULL,
    tipo ENUM('primo','panino','pizza') NOT NULL,
    base VARCHAR(255) NOT NULL,
    base2 VARCHAR(255) NOT NULL,
    prezzo FLOAT(7,2) NOT NULL,
    ing1 VARCHAR(255),
    ing2 VARCHAR(255),
    ing3 VARCHAR(255),
    ing4 VARCHAR(255),
    ing5 VARCHAR(255),
    ing6 VARCHAR(255),
    aggiunta_formaggio ENUM('Y','N') NOT NULL,
    sconto ENUM('Y','N') NOT NULL,
    PRIMARY KEY(id)
    )";
    // use exec() because no results are returned
    $conn->exec($sq3);
    echo "Tabella piatto creato con successo </br>";
//-------------------------------------------------------------
// TABELLA PIATTI SECONDARI
    $sq4 = "CREATE TABLE IF NOT EXISTS vario (
    id INT AUTO_INCREMENT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    tipo ENUM('contorno','dessert','stuzzichino', 'bibita') NOT NULL,
    prezzo FLOAT(7,2) NOT NULL,
    sconto ENUM('Y','N') NOT NULL,
    PRIMARY KEY(id)
    )";
// use exec() because no results are returned
    $conn->exec($sq4);
    echo "Tabella vario creato con successo </br>";
//-------------------------------------------------------------
// ELEMENTO TABLE
    $sq5 = "CREATE TABLE IF NOT EXISTS elemento (
    nome VARCHAR(255) NOT NULL,
    tipo ENUM('pasta','sugo','pane','base_pizza','farina','ingrediente','salsa') NOT NULL,
    per_panino ENUM('Y','N') NOT NULL,
    per_pizza ENUM('Y','N') NOT NULL,
    add_price FLOAT(7,2) NOT NULL,
    PRIMARY KEY(nome)
)";
// use exec() because no results are returned
    $conn->exec($sq5);
    echo "Tabella elemento creato con successo </br>";
//--------------------------------------------------------------
// ORDINI TABLE
    $sql6 = "CREATE TABLE IF NOT EXISTS ordine (
    id INT AUTO_INCREMENT NOT NULL,
    username VARCHAR(255) NOT NULL,
    prezzo FLOAT(7,2) NOT NULL,
    stato ENUM('inviato','ricevuto','esecuzione','pronto','archivia') NOT NULL,
    visto ENUM('Y','N') NOT NULL,
    PRIMARY KEY(id)
    )";
    // use exec() because no results are returned
    $conn->exec($sql6);
    echo "Tabella ordine creato con successo </br>";
//--------------------------------------------------------------
// PRODOTTO IN ORDINE TABLE
    $sql7 = "CREATE TABLE IF NOT EXISTS prodottoInOrdine (
    id_ordine INT NOT NULL,
    prodotto VARCHAR(255) NOT NULL,
    tipo VARCHAR(255) NOT NULL,
    quantita INT NOT NULL,
    PRIMARY KEY(id_ordine,prodotto)
    )";
    // use exec() because no results are returned
    $conn->exec($sql7);
    echo "Tabella prodottoInOrdine creato con successo </br>";

    // NEWS
    $sql8 = "CREATE TABLE IF NOT EXISTS news (
    id INT AUTO_INCREMENT NOT NULL,
    titolo VARCHAR(255) NOT NULL,
    testo LONGTEXT NOT NULL,
    tempo DATETIME NOT NULL,
    PRIMARY KEY(id)
    )";
    // use exec() because no results are returned
    $conn->exec($sql8);
    echo "Tabella news creato con successo </br>";


} catch (PDOException $e) {
    echo $e->getMessage();
}
//close connection
$conn = null;
?>
