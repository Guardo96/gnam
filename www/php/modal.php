<div class="modal fade" id="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">...</h2>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="modcontent"></p>
      </div>
      <div class="modal-footer divbtn">
        <div class="collapse" id="success">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cambiaPagina()">Torna alla Home</button>
        </div>
        <div class="collapse" id="wrong">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Torna al form</button>
        </div>
      </div>
    </div>
  </div>
</div>
