<?php

$database = include('../db/dbconfig.php');

extract($_GET);


try {
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if($code == "ordine"){
      $stmt = $conn->prepare("INSERT INTO ordine (id, username, prezzo, stato, visto)
                                    VALUES (:id, :username, :prezzo, :stato, 'N')");
      $stmt->bindParam(':id', $id);
      $stmt->bindParam(':username', $username);
      $stmt->bindParam(':prezzo', $prezzo);
      $stmt->bindParam(':stato', $stato);
      $stmt->execute();

      $statement = $conn->prepare("SELECT * FROM ordine WHERE username = '$username' ORDER BY id DESC LIMIT 1");
      $statement->execute();
      $row = $statement->fetch();
      echo $row['id'] ;
    }
    if($code == "prodotto"){
        $stmt = $conn->prepare("INSERT INTO prodottoInOrdine (id_ordine, prodotto, tipo, quantita)
                                VALUES (:id_ordine, :prod, :tipo, :qta)");
      $stmt->bindParam(':id_ordine', $id_ordine);
      $stmt->bindParam(':prod', $prod);
      $stmt->bindParam(':tipo', $tipo);
      $stmt->bindParam(':qta', $qta);
      $stmt->execute();

  }
} catch (PDOException $e) {
     echo "Error: " . $e->getMessage();
}

?>
