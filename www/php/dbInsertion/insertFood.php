<?php

$database = include('../db/dbconfig.php');


extract($_GET);
$sconto = "N";


try {
    //connect to the database
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("INSERT INTO piatto (id, username, nome, tipo, base, base2, prezzo, ing1, ing2, ing3, ing4, ing5, ing6, aggiunta_formaggio, sconto)
                                    VALUES (:id, :username, :nome, :tipo, :base, :base2, :prezzo, :ing1, :ing2, :ing3, :ing4, :ing5, :ing6, :aggiunta_formaggio, 'N')");
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':base', $base);
    $stmt->bindParam(':base2', $base2);
    $stmt->bindParam(':prezzo', $prezzo);
    $stmt->bindParam(':ing1', $ing1);
    $stmt->bindParam(':ing2', $ing2);
    $stmt->bindParam(':ing3', $ing3);
    $stmt->bindParam(':ing4', $ing4);
    $stmt->bindParam(':ing5', $ing5);
    $stmt->bindParam(':ing6', $ing6);
    $stmt->bindParam(':aggiunta_formaggio', $aggiunta_formaggio);

    $stmt->execute();
    echo "Inserito nuovo piatto!";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
