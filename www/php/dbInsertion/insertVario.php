<?php

$database = include('../db/dbconfig.php');


extract($_GET);



try {
    //connect to the database
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("INSERT INTO vario (id, nome, tipo, prezzo, sconto)
                                    VALUES (:id, :nome, :tipo, :prezzo, 'N')");
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':tipo', $tipo);
    $stmt->bindParam(':prezzo', $prezzo);

    $stmt->execute();
    echo "Inserito nuovo piatto!";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
