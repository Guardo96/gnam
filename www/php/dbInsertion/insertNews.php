<?php
$database = include('../db/dbconfig.php');

extract($_POST);

$dt = date("d-m-Y H:i:s", time());
var_dump($dt);

try {
    //connect to the database
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $conn->prepare("INSERT INTO news (titolo, testo, tempo) VALUES (:titolo,:testo,NOW())");
    $query->bindParam(':titolo', $titolo);
    $query->bindParam(':testo', $testo);
    $query->execute();
    echo "Inserita nuova notizia!";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
