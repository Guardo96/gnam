<?php

$database = include('../db/dbconfig.php');

$ingredient = $_GET['ingrName'];
$ingr_type = $_GET['type'];
$per_pizza = $_GET['per_pizza'];
$per_panino = $_GET['per_panino'];
$add_price = $_GET['euro'];
try{
  //connect to the database
  $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //prepare the statement
  $stmt = $conn->prepare("INSERT INTO elemento(nome, tipo, per_panino, per_pizza, add_price)
                         VALUES (:ingredient,:ingr_type,:per_panino,:per_pizza,:add_price)");
  $stmt->bindParam(':ingredient', $ingredient);
  $stmt->bindParam(':ingr_type', $ingr_type);
  $stmt->bindParam(':per_panino', $per_panino);
  $stmt->bindParam(':per_pizza', $per_pizza);
  $stmt->bindParam(':add_price', $add_price);
  $stmt->execute();

  echo "Inserito nuovo ingrediente!";
} catch(PDOException $e){
  echo "Error" . $e->getMessage();
}
 ?>
