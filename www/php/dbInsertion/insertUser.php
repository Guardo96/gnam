<?php

$database = include('../db/dbconfig.php');

$username = $_POST['username'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$address = $_POST['address'];
$password = $_POST['password'];
$carta = $_POST['carta'];

//password hash

$password = password_hash($password, PASSWORD_DEFAULT);

try {
    //connect to the database
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check if username already exist
    $query = $conn->prepare("SELECT username FROM utente WHERE username=:username");
    $query->bindParam(':username',$username);
    $query->execute();
    if ($query->rowCount() == 0){
      //insert new record
        $stmt = $conn->prepare("INSERT INTO utente (username,nome,cognome,indirizzo,carta,email,password)
                             VALUES (:username,:firstname,:lastname,:address,:carta,:email,:password)");
      $stmt->bindParam(':username', $username);
      $stmt->bindParam(':firstname', $firstname);
      $stmt->bindParam(':lastname', $lastname);
      $stmt->bindParam(':address', $address);
        $stmt->bindParam(':carta', $carta);
      $stmt->bindParam(':email', $email);
      $stmt->bindParam(':password', $password);
      $stmt->execute();

      echo "New record created successfully";
    } else {
      echo "username non valido";
    }
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
} 
?>
