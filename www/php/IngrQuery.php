<?php
$piatto = $_GET['value'];
$sezione = $_GET['sez'];
  try {
      $database = include('db/dbconfig.php');

      $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      if($sezione != "ingr"){
        $statement = $conn->prepare("SELECT * FROM elemento WHERE tipo = '$sezione'");
        $statement->execute();
        while ($row = $statement->fetch()) {
          echo "<option>";
          echo $row['nome'];
          echo "</option>";
        }
      }else {
        if($piatto == "pizza"){
            $statement = $conn->prepare("SELECT * FROM elemento WHERE tipo = 'ingrediente' AND per_pizza = 'Y'");
            $statement->execute();
            while ($row = $statement->fetch()) {
              echo "<option>";
              echo $row['nome'] ;
              echo "</option>";
            }
        } else {
          $statement = $conn->prepare("SELECT * FROM elemento WHERE tipo = 'ingrediente' AND per_panino = 'Y'");
          $statement->execute();
          while ($row = $statement->fetch()) {
            echo "<option>";
            echo $row['nome'];
            echo "</option>";
          }
        }
      }
  } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
  }
?>
