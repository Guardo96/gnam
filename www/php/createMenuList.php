<div class="container">
  <?php
  $database = include('db/dbconfig.php');
  try {
      $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $statement = $conn->prepare("SELECT * FROM menu WHERE username = 'admin'");
      $statement->execute();
   } catch (PDOException $e) {
         echo "Error: " . $e->getMessage();
  }

  $i = 0;
  while ($row = $statement->fetch()) {
    include('menu.php');
    $i++;
  }
  ?>
<div id="numeroProdotti" class="collapse"> <?php echo $i ?></div>
</div>
