<?php $fileIngr = "php/IngrQuery.php"; ?>
<div class="container">
  <div class="center-block">
    <form>
      <label for="nomePiatto">Nome piatto: </label>
      <input type="text" id="nomePiatto" name="nomePiatto" class="form-control input-sm" required="required"/><br/>
      <?php if($_GET['categoria'] == 'secondo') {
        include('secondo.php');
      }
      ?>
<!--                                                                -->
      <?php if ($_GET['categoria'] == 'primo') {
        include('primo.php');
      }
      ?>
      <div class="divbtn">
        <input type="button" id="creaPiatto" class="btn btn-primary" onclick="insertDish()" data-toggle="modal" data-target="#modal" value="Crea piatto"/>
      </div>
      <?php include("modal.php"); ?>
    </form>
  </div>
</div>

<?php
// Funzione usata per riempire la select degli ingredienti
function aggiungiElemento($elem) {
  try {
      //connect to the database
      $database = include('db/dbconfig.php');
      $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $statement = $conn->prepare("SELECT nome FROM elemento WHERE tipo = '$elem'");
      $statement->execute();
      while ($row = $statement->fetch()) {
        echo "<option>";
        echo $row['nome'] ;
        echo" </option>";
      }
  } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
  }
}
?>
