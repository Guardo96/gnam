<?php
extract($_GET);
try {
  $database = include('../php/db/dbconfig.php');
  $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  switch($tipo){
    case "nonPronti":
      $statement = $conn->prepare("SELECT * FROM ordine WHERE (stato = 'inviato' OR stato = 'ricevuto') AND username = '$user'");
      break;
    case "esecuzione":
      $statement = $conn->prepare("SELECT * FROM ordine WHERE stato = 'esecuzione' AND username = '$user'");
      break;
    case "pronti":
      $statement = $conn->prepare("SELECT * FROM ordine WHERE stato = 'pronto' AND username = '$user'");
      break;
   case "archivia":
      $statement = $conn->prepare("UPDATE ordine SET stato = 'archivia' WHERE id = $id");
      break;
    default:
    break;
  }
  $statement->execute();
  $count = $statement->rowCount();
  if ($count == 0) {
      echo "non ci sono ordini";
  } else {
      while ($row = $statement->fetch()) {
          $i = $row['id'];
          $statement2 = $conn->prepare("SELECT * FROM prodottoInOrdine WHERE id_ordine = '$i'");
          $statement2->execute();
          $count2 = $statement2->rowCount();
          if ($count2 == 0) {
            echo "Errore ";
          } else { ?>
          <section class="food<?php echo $i; ?>">
          <p id="ordine<?php echo $i; ?>" class="pOrdine"><span>Ordine N <?php echo $row['id']; ?></span></p>
          <p id="status<?php echo $i; ?>" class="pOrdine"><span>Stato: <?php echo $row['stato']; ?></span></p>
          <div id="info<?php echo $i; ?>" class="divInfo">
          <ul>
          <?php while ($row2 = $statement2->fetch()) { ?>
          <p id="prodotto<?php echo $i; ?>" class="pprodotto"><li> <?php echo $row2['prodotto']; ?>
          <?php if($row2['quantita']>1){?> x<?php echo $row2['quantita']; } ?></li></p> <?php } ?>
          </ul>
          <p id="price<?php echo $i; ?>" class="pPrice"><?php echo $row['prezzo']; ?> €</p>
          <?php if($row['stato'] == "pronto"){ ?>
          <input type="button" id="esec<?php echo $i; ?>" class="btn btn-primary" onclick="archivia(<?php echo $i; ?>)" value="Archivia"/>
          <?php } ?>
          <p>-------------------------------------------------------</p>
          </div>
          </section>
          <?php
          }
          if($tipo == "esecuzione" || $tipo == "pronti"){
            $statement2 = $conn->prepare("UPDATE ordine SET visto = 'Y' WHERE id = '$i'");
            $statement2->execute();
          }
    }
  }
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
