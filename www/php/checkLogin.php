<?php
session_start();
$database = include('db/dbconfig.php');
$email = $_POST['email'];
$password1 = $_POST['password'];

try {
    //connect to the database
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $conn->prepare('SELECT * FROM utente WHERE email = :email');
    $statement->bindParam(':email', $email, PDO::PARAM_STR);
    $statement->execute();

    $count = $statement->rowCount();
    if ($count == 0) {
        header("location: ../login.php");
    } else {
        while ($row = $statement->fetch()) {
            if (password_verify($password1, $row['password'])) {
                $_SESSION['user'] = $row['username'];

                if (isset($_POST['ricordami'])) {
                    setcookie("rememberme", $row['username'], time() + (86400 * 30), "/");
                }
                if ($_SESSION['user'] == "admin") {
                    header("location: ../admin/adminEntryPage.php");
                } else {
                    header("location: ../mainMenu.php?");
                }
            } else {
                header("location: ../login.php");
            }
        }
    }
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
