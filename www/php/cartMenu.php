<?php
$cat = "prod";
$tag = "$cat$numero";
?>


<div class="menu" id="<?php echo $tag ?>">
  <div class="name" id="nome<?php echo $tag ?>"><?php echo $row["nome"] ?></div>
  <div class="divOrdine">
    <div class="divProd">
      <div class="divTable">
        <table class="table">
          <caption class="collapse">Piatti che compongono il menù</caption>
          <tbody>
          <?php if($row["primo"] != null && $row["primo"] != ""  && $row["primo"] != "No primo"){ ?>
            <tr>
              <th id="mPrimo" scope="row">Primo</th>
              <td headers="mPrimo"><?php echo $row["primo"] ?></td>
            </tr>
            <tr>
              <th class="rigaFormaggio" id="mFormaggio" scope="row">Con formaggio</th>
              <td headers="mFormaggio">
                <label for="qtaForm<?php echo $tag ?>">con formaggio:</label>
                <input type="number" min="0" max="<?php echo $quantita ?>" value="0" id="qtaForm<?php echo $tag ?>"/>
              </td>
            </tr>
           <?php } ?>
           <?php if($row["secondo"] != null  && $row["secondo"] != "" && $row["secondo"] != "No secondo"){ ?>
            <tr>
              <th id="mSec" scope="row">Secondo</th>
              <td headers="mSec"><?php echo $row["secondo"] ?></td>
            </tr>
          <?php } ?>
          <?php if($row["contorno"] != null && $row["contorno"] != ""  && $row["contorno"] != "No contorno"){ ?>
            <tr>
              <th id="mCont" scope="row">Contorno</th>
              <td headers="mCont"><?php echo $row["contorno"] ?></td>
            </tr>
          <?php } ?>
          <?php if($row["stuzzichino"] != null  && $row["stuzzichino"] != ""  && $row["stuzzichino"] != "No stuzzichino"){ ?>
            <tr>
              <th id="mSt" scope="row">Stuzzichino</th>
              <td headers="mSt"><?php echo $row["stuzzichino"] ?></td>
            </tr>
          <?php } ?>
          <?php if($row["dessert"] != null && $row["dessert"] != ""  && $row["dessert"] != "No dessert"){ ?>
            <tr>
              <th id="mDess" scope="row">Dessert</th>
              <td headers="mDess"><?php echo $row["dessert"] ?></td>
            </tr>
          <?php } ?>
          <?php if($row["bibita"] != null && $row["bibita"] != "" && $row["bibita"] != "No bibita"){ ?>
            <tr>
              <th id="mDrink" scope="row">Bibita</th>
              <td headers="mDrink"><?php echo $row["bibita"] ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="prezzo" id="prezzouni<?php echo $tag ?>">Prezzo unitario: <?php echo $row["prezzo"] ?> €</div>
      <div class="qta">
        <label for="menuQta">Quantità: </label>
        <input type="number" min="1" max="99" value="<?php echo $quantita; ?>" id="numQta<?php echo $tag ?>" onchange="cambiaQuantita('<?php echo $tag ?>')"/>
      </div>
      <div class="totale" id="prezzo<?php echo $tag ?>">Prezzo totale: <?php echo ($quantita*$row["prezzo"]) ?> €</div>
      <div class="elimina">
        <input type="button" class="btn btn-outline-primary btn-sm elim" value="Elimina menù" onclick="rimuovi('<?php echo $tag ?>')"/>
      </div>
    </div>
  </div>
</div>
