<fieldset>
  <legend class="collapse">Scelta base</legend>
  <div class="custom-control custom-radio">
    <input type="radio" id="panino" name="scegli" value="base_panino" class="custom-control-input" onclick="sceltaBase('panino', '<?php echo $fileIngr; ?>')" />
    <label for="panino" class="custom-control-label">Panino</label><br/>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" id="pizza" name="scegli" value="base_pizza" class="custom-control-input" onclick="sceltaBase('pizza', '<?php echo $fileIngr; ?>')" />
    <label for="pizza" class="custom-control-label">Pizza</label>
  </div><br/>
</fieldset>
<!-- Select per base secondo -->
<div class="collapse" id="divBSecondo">
  <label for="base1s">Base1:</label>
  <select id="base1s" class="form-control">
  </select><br/>
  <label for="base2s">Base2:</label>
  <select id="base2s" class="form-control">
  </select><br/>
  <!-- Select per ingrediente  -->
  <div id="divMultiIng">
    <?php for($i = 0; $i < 6; $i++) { ?>
      <div id="ing<?php echo $i; ?>" class="collapse">
        <label for="ingrediente<?php echo $i; ?>">Scegli <?php $j = $i + 1; echo $j; ?>° ingrediente:</label>
        <select id="ingrediente<?php echo $i; ?>" class="form-control">
        </select><br/>
      </div>
    <?php } ?>
    <div id="numIng" class="collapse">0</div>
  </div>
  <div class="divbtn" id="divbtningr">
    <input type="button" id="btnAdd" class="btn btn-outline-primary" value="Aggiungi ingrediente" onclick="addIngr('<?php echo $fileIngr; ?>')"/>
    <input type="button" id="btnRem" class="btn btn-outline-primary" value="Rimuovi ingrediente" onclick="removeIngr()"/>
  </div>
  <div class="priceInfo" id="priceInfoS">Prezzo: </div>
</div>
