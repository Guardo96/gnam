<?php
$database = include('db/dbconfig.php');

extract($_GET);

try {
    $conn = new PDO("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if($tipo == "tutti"){
      $statement = $conn->prepare("SELECT * FROM piatto WHERE (tipo = 'panino' OR tipo = 'pizza') AND (username = 'admin' OR username = '$user')");
    } else {
      $statement = $conn->prepare("SELECT * FROM piatto WHERE tipo = '$tipo'  AND (username = 'admin' OR username = '$user' )");
    }
    $statement->execute();

    $i = 0;
    while ($row = $statement->fetch()) { ?>
      <div class="food">
        <div id="info<?php echo $i; ?>" class="divInfo">
          <div class="vedisconto">
            <?php if($row['sconto'] == "Y"): ?>
            <div class="insconto">
              <span class="fas fa-exclamation"> </span>
            </div>
            <?php endif; ?>
            <p id="name<?php echo $i; ?>" class="pName"><?php echo $row['nome']; ?></p>
          </div>
          <p id="price<?php echo $i; ?>" class="pPrice"><?php echo $row['prezzo']; ?> €</p>
        </div>
        <div id="amount<?php echo $i; ?>" class="divAmount">
          <button id="less<?php echo $i; ?>" class=" buttonLess btn btn-primary btn-circle" onclick="remove(<?php echo $i; ?>)"><span class="fa fa-minus"></span></button>
          <div id="num<?php echo $i; ?>" class="divNum">0</div>
          <button id="more<?php echo $i; ?>" class=" buttonMore btn btn-primary btn-circle" onclick="add(<?php echo $i; ?>)"><span class="fa fa-plus"></span></button>
        </div>
      </div>
  <?php
  $i++;
} ?>
<div id="numeroProdotti" class="collapse"><?php echo $i; ?></div>
<?php
   } catch (PDOException $e) {
       echo "Error: " . $e->getMessage();
   }

?>
