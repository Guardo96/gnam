
<?php
 $cat = "prod";
 $tag = "$cat$numero";
?>
<div class="prod" id="<?php echo $tag ?>">
  <div class="divOrdine">
    <div class="divProd">
        <div class="prodotto" id="dati<?php echo $tag ?>" <?php if($tipo == "primo") { echo 'class="prodprimo"';} ?>>
        <div id="nome<?php echo $tag ?>"><?php echo $row["nome"];?></div>
          <?php if($tipo == "primo" && $row["aggiunta_formaggio"] == "Y"){ ?>
              <div class="formaggio">
                 <label for="qtaForm<?php echo $tag ?>">con formaggio:</label>
                 <input type="number" min="0" max="<?php echo $quantita ?>" value="0" id="qtaForm<?php echo $tag ?>" />
               </div>
          <?php } ?>
        </div>
        <div class="prezzo" id="prezzouni<?php echo $tag ?>">Prezzo unitario: <?php echo $row["prezzo"] ?> €</div>
          <div class="qta">
            <label for="numQta<?php echo $tag ?>">Quantità: </label>
            <input type="number" min="1" max="99" value="<?php echo $quantita ?>" id="numQta<?php echo $tag ?>" onchange="cambiaQuantita('<?php echo $tag ?>')"/>
          </div>
        <div class="totale" id="prezzo<?php echo $tag ?>">Prezzo totale: <?php echo ($quantita*$row["prezzo"]); ?> €</div>
        <div class="elimina">
              <input type="button" class="btn btn-outline-primary btn-sm elim" value="Elimina prodotto" onclick="rimuovi('<?php echo $tag ?>')"/>
        </div>
      </div>
    </div>
</div>
